
# o diret�rio de scripts � o diret�rio onde este arquivo se encontra
SCRIPT_DIR = File.expand_path( File.dirname(__FILE__) )
# o diret�rio raiz do projeto est� um n�vel acima do diret�rio de scripts
PROJECT_ROOT_DIR = File.expand_path( SCRIPT_DIR + '/..' )

VHDL_ROOT_DIR = File.expand_path( PROJECT_ROOT_DIR + '/vhdl' )
VHDL_SYNTH_DIR = File.expand_path( VHDL_ROOT_DIR + '/synth' )
VHDL_TESTBENCH_DIR = File.expand_path( VHDL_ROOT_DIR + '/tb' )
 

if ARGV.size == 1 && ARGV[0] == '-t'
  puts 'Compiling testbenches only.'
  @compile_testbenches_only = true
end

def compile_vhdl_file(file_name)
  puts "Compiling '#{file_name}'..."
  vcom_output = `vcom #{file_name} -2008 -work modelsim/work`
  vcom_output = `vcom #{file_name} -work modelsim/work`
  if vcom_output.match(/\*\* Error/)
   error_message = vcom_output[/(\*\* Error.*$)/m]
    raise error_message
  end
  puts vcom_output
end

def simulate_vhdl_entity(entity, architecture)
  target = '"' + entity + '(' + architecture + ')"' 
  vsim_output = `vsim -c #{target} -do "run -all;quit"`
  puts vsim_output
end

def compile_vhdl_files(path)
  filenames = Dir.glob(path)
  puts "Found VHDL files:"
  puts filenames.join("\n")
  failed_filenames = []
  begin
    previously_failed_filenames = failed_filenames  
    failed_filenames.clear
    filenames.each do |filename|
      begin 
        compile_vhdl_file(filename)
      rescue
        puts "#{$!}"
        failed_filenames << filename
      end
    end
    p failed_filenames
  end while failed_filenames.any? and (failed_filenames != previously_failed_filenames)
    
  if failed_filenames.any?
    puts "*** Error: (pon2vhdl) Failed to compile:"
    puts failed_filenames.join("\n")
    exit
  end
end

unless @compile_testbenches_only
  # compile all synthesizeable VHDL files
  puts "Compiling synthesizeable VHDL files..."
  compile_vhdl_files("#{VHDL_SYNTH_DIR}/**/*.vhd")
end
  
# compile all testbench VHDL files
puts "Compiling VHDL testbehch files..."
compile_vhdl_files("#{VHDL_TESTBENCH_DIR}/**/*.vhd")

# run all testbenches
testbench_executable_filenames = Dir.glob("#{VHDL_TESTBENCH_DIR}/**/*_tb.vhd")
puts "Running VHDL testbehch files:"
puts testbench_executable_filenames.join("\n")
testbench_executable_filenames.each do |file_name|
  entity_name = File.basename(file_name, '.vhd')
  simulate_vhdl_entity(entity_name, 'testbench')
end
