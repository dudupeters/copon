library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity copon is
	generic(
		ADDRESS_WIDTH        : integer := 5;
		DATA_WIDTH           : integer := 32;
		NUMBER_OF_PREMISSES  : integer := 32;
		NUMBER_OF_CONDITIONS : integer := 2
	);
	port(
		clk, reset   : in  std_logic;
		address      : in  std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
		chip_select  : in  std_logic;
		write_enable : in  std_logic;
		write_data   : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		read_enable  : in  std_logic;
		read_data    : out std_logic_vector(DATA_WIDTH - 1 downto 0)
	);
end copon;

architecture arch of copon is
	constant REGISTERS_COUNT : integer := 18;

	subtype word is std_logic_vector(DATA_WIDTH - 1 downto 0);
	type memory is array (0 to REGISTERS_COUNT - 1) of word;
	signal registers : memory;

	signal atttribute0_value, atttribute1_value : word;

	signal premise0_const_value, premise1_const_value : word;
	signal premise2_const_value, premise3_const_value : word;

	signal premise0_attribute_value, premise1_attribute_value : word;
	signal premise2_attribute_value, premise3_attribute_value : word;

	signal premise0_config, premise1_config : word;
	signal premise2_config, premise3_config : word;

	signal premises_output : std_logic_vector(NUMBER_OF_PREMISSES - 1 downto 0);

	signal premise0_src_sel, premise1_src_sel : std_logic;
	signal premise2_src_sel, premise3_src_sel : std_logic;

	signal condition0_needed_premises, condition0_rule_addr : word;
	signal condition1_needed_premises, condition1_rule_addr : word;

	signal rule_ready : word;

	signal conditions_output : std_logic_vector(NUMBER_OF_CONDITIONS - 1 downto 0);

begin
	process(clk, reset)
	begin
		if reset = '1' then
			registers <=(others => (others => '0'));
		elsif(clk'event and clk = '1') then
			if write_enable = '1' and chip_select = '1' then
				registers(to_integer(unsigned(address))) <= write_data;
			end if;
			if read_enable = '1' and chip_select = '1' then
				read_data <= registers(to_integer(unsigned(address)));
			end if;
		end if;
	end process;

	atttribute0_value          <= registers(2);
	atttribute1_value          <= registers(3);
	premise0_const_value       <= registers(4);
	premise0_config            <= registers(5);
	premise1_const_value       <= registers(6);
	premise1_config            <= registers(7);
	premise2_const_value       <= registers(8);
	premise2_config            <= registers(9);
	premise3_const_value       <= registers(10);
	premise3_config            <= registers(11);
	condition0_needed_premises <= registers(12);
	condition0_rule_addr       <= registers(13);
	condition1_needed_premises <= registers(14);
	condition1_rule_addr       <= registers(15);

	rule_ready <= registers(17);

	premise0_src_sel <= premise0_config(3);
	premise1_src_sel <= premise1_config(3);
	premise2_src_sel <= premise2_config(3);
	premise3_src_sel <= premise3_config(3);

	-- o atributo usado como entrada pela premissa depende do 
	-- bit de configuração src_sel
	premise0_attribute_value <= atttribute0_value when premise0_src_sel = '0' else
		atttribute1_value;
	premise1_attribute_value <= atttribute0_value when premise1_src_sel = '0' else
		atttribute1_value;
	premise2_attribute_value <= atttribute0_value when premise2_src_sel = '0' else
		atttribute1_value;
	premise3_attribute_value <= atttribute0_value when premise3_src_sel = '0' else
		atttribute1_value;

	premise0 : entity work.premise
		port map(
			attribute_value => premise0_attribute_value,
			const_value     => premise0_const_value,
			operation_type  => premise0_config(2 downto 0),
			output          => premises_output(0)
		);

	premise1 : entity work.premise
		port map(
			attribute_value => premise1_attribute_value,
			const_value     => premise1_const_value,
			operation_type  => premise1_config(2 downto 0),
			output          => premises_output(1)
		);
	premise2 : entity work.premise
		port map(
			attribute_value => premise2_attribute_value,
			const_value     => premise2_const_value,
			operation_type  => premise2_config(2 downto 0),
			output          => premises_output(2)
		);
	premise3 : entity work.premise
		port map(
			attribute_value => premise3_attribute_value,
			const_value     => premise3_const_value,
			operation_type  => premise3_config(2 downto 0),
			output          => premises_output(3)
		);

	condition0 : entity work.condition
		port map(
			active_premises => premises_output,
			needed_premises => condition0_needed_premises,
			rule_addr       => condition0_rule_addr,
			output          => conditions_output(0)
		);

	condition1 : entity work.condition
		port map(
			active_premises => premises_output,
			needed_premises => condition1_needed_premises,
			rule_addr       => condition1_rule_addr,
			output          => conditions_output(1)
		);

	process(conditions_output)
	begin
		rule_ready(1 downto 0) <= conditions_output; --std_logic_vector(to_signed(conditions_output, NUMBER_OF_CONDITIONS));
	end process;
end arch;