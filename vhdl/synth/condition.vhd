library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity condition is
	generic(
		DATA_WIDTH          : integer := 32;
		NUMBER_OF_PREMISSES : integer := 32
	);
	port(
		active_premises : std_logic_vector(NUMBER_OF_PREMISSES - 1 downto 0);
		needed_premises : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		rule_addr       : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		output          : out std_logic
	);
end entity;

architecture behavior of condition is
	signal op1, op2 : signed(DATA_WIDTH - 1 downto 0);
begin
	process(active_premises, needed_premises)
	begin
		if active_premises = needed_premises then
			output <= '1';
		else
			output <= '0';
		end if;

	end process;

end architecture;
