library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity premise is
	generic(
		DATA_WIDTH    : integer := 32
	);
	port(
		attribute_value : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		const_value     : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		operation_type  : in  std_logic_vector(2 downto 0);
		output          : out std_logic
	);
end entity;

architecture behavior of premise is
	signal op1, op2 : signed(DATA_WIDTH - 1 downto 0);

	function std_logic_from_boolean(bool : boolean) return std_logic is
	begin
		if bool then
			return '1';
		else
			return '0';
		end if;
	end function;

begin
	op1 <= signed(attribute_value);
	op2 <= signed(const_value);

	process(op1, op2, operation_type)
	begin
		case operation_type is
			when "000"  => output <= std_logic_from_boolean(op1 = op2);
			when "001"  => output <= std_logic_from_boolean(op1 /= op2);
			when "010"  => output <= std_logic_from_boolean(op1 < op2);
			when "011"  => output <= std_logic_from_boolean(op1 <= op2);
			when "100"  => output <= std_logic_from_boolean(op1 > op2);
			when "101"  => output <= std_logic_from_boolean(op1 >= op2);
			when others => output <= '0';
		end case;
	end process;
end architecture;
