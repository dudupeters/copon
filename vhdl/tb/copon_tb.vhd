library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.testbench_utils.all;

entity copon_tb is
	generic(
		ADDRESS_WIDTH       : integer := 5;
		DATA_WIDTH          : integer := 32;
		NUMBER_OF_PREMISSES : integer := 32
	);
end entity;                             --copon_tb;

architecture testbench of copon_tb is
	constant MINIMUM_CLOCK_PERIOD : time := 10 ns;
	constant CLOCK_DUTY_CYCLE     : real := 0.5;

	signal clk          : std_logic                                    := '0';
	signal reset        : std_logic                                    := '0';
	signal address      : std_logic_vector(ADDRESS_WIDTH - 1 downto 0) := (others => '0');
	signal chip_select  : std_logic;
	signal write_enable : std_logic;
	signal write_data   : std_logic_vector(DATA_WIDTH - 1 downto 0)    := (others => '0');
	signal read_enable  : std_logic;
	signal read_data    : std_logic_vector(DATA_WIDTH - 1 downto 0)    := (others => '0');

	-- DUT --> Testench

	function int2std_logic_vector(int : integer) return std_logic_vector is
	begin
			return std_logic_vector(to_signed(int, DATA_WIDTH));
	end function;

begin
	dut_copon : entity work.copon(arch)
		generic map(
			ADDRESS_WIDTH => ADDRESS_WIDTH,
			DATA_WIDTH    => DATA_WIDTH
		) port map(
			clk => clk,
			reset => reset,
			address => address,
			chip_select => chip_select,
			write_enable => write_enable,
			write_data => write_data,
			read_enable => read_enable,
			read_data => read_data
		);

	run_test_cases : process is
		procedure pulse_clock is
		begin
			clk <= '0';
			wait for MINIMUM_CLOCK_PERIOD *(1.0 - CLOCK_DUTY_CYCLE);
			clk <= '1';
			wait for MINIMUM_CLOCK_PERIOD * CLOCK_DUTY_CYCLE;
		end procedure;

		procedure write_register(addr : integer; data : integer) is
		begin
			chip_select  <= '1';
			write_enable <= '1';
			address      <= std_logic_vector(to_signed(addr, ADDRESS_WIDTH));
			write_data   <= std_logic_vector(to_signed(data, DATA_WIDTH));
			pulse_clock;
			chip_select  <= '0';
			write_enable <= '0';
		end procedure;

		procedure read_register(addr : integer) is
			variable readed_data : integer;
		begin
			address     <= std_logic_vector(to_signed(addr, ADDRESS_WIDTH));
			chip_select <= '1';
			read_enable <= '1';
			pulse_clock;
			chip_select <= '0';
			read_enable <= '0';
		end procedure;

		procedure test_rw_registers is
			variable a : std_logic_vector(DATA_WIDTH - 1 downto 0);
		begin
			puts("Testing reading and writing to registers");
			write_register(1, 123456789);
			write_register(15, 987654321);
			read_register(1);
			should("register 1 should be 123456789", read_data =int2std_logic_vector(123456789));
			read_register(15);
			should("register 15 should be 987654321", read_data =int2std_logic_vector(987654321));
			
		end procedure;

	begin
		test_rw_registers;

		wait;
	end process;

end architecture testbench;
