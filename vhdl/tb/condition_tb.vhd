library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.testbench_utils.all;

entity condition_tb is
	generic(
		DATA_WIDTH : integer := 32;
		NUMBER_OF_PREMISSES: integer := 32
	);
end entity;

architecture testbench of condition_tb is

	-- Testbench --> DUT
	signal premises   : std_logic_vector(NUMBER_OF_PREMISSES - 1 downto 0) := (others => '0');
	signal conditions : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');
	signal rule_addr  : std_logic_vector(DATA_WIDTH - 1 downto 0);

	-- DUT --> Testench
	signal output : std_logic;

	signal int_premises   : integer := 0;
	signal int_conditions : integer := 0;
	signal int_rule_addr  : integer := 0;
begin
	dut_condition : entity work.condition(behavior)
		generic map(
			DATA_WIDTH => DATA_WIDTH
		) port map(
			active_premises => premises,
			needed_premises => conditions,
			rule_addr => rule_addr,
			output => output
		);

	premises   <= std_logic_vector(to_signed(int_premises, NUMBER_OF_PREMISSES));
	conditions <= std_logic_vector(to_signed(int_conditions, DATA_WIDTH));
	rule_addr  <= std_logic_vector(to_signed(int_rule_addr, DATA_WIDTH));

	run_test_cases : process is
		procedure test_condition is
		begin
			puts("Testing condition output");
			int_premises   <= 10;
			int_conditions <= 10;
			short_wait;
			should("be high when premisses and condition's premisse are equals", output = '1');

			int_premises   <= 10;
			int_conditions <= 11;
			short_wait;
			should("be low when premisses and condition's premisse are not equals", output = '0');

		end procedure;

	begin
		test_condition;
		wait;
	end process;

end architecture;