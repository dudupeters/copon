library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.testbench_utils.all;

entity premise_tb is
	generic(
		DATA_WIDTH : integer := 32
	);
end entity;

architecture testbench of premise_tb is

	-- Testbench --> DUT
	signal attribute_value : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');
	signal const_value     : std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');
	signal operation_type  : std_logic_vector(2 downto 0);

	-- DUT --> Testench
	signal output : std_logic;
	
	signal int_attribute_value : integer := 0;
	signal int_const_value     : integer := 0;
	
begin
	dut_premise : entity work.premise(behavior)
		generic map(
			DATA_WIDTH => DATA_WIDTH
		) port map(
			attribute_value => attribute_value,
			const_value => const_value,
			operation_type => operation_type,
			output => output
		);

	attribute_value <= std_logic_vector(to_signed(int_attribute_value, DATA_WIDTH));
	const_value <= std_logic_vector(to_signed(int_const_value, DATA_WIDTH));
	
	run_test_cases : process is
		procedure test_premise is
		begin
			puts("Testing premise output");
			int_attribute_value <= 10;
			int_const_value     <= 10;
			operation_type <= "000";
			short_wait;
			should("be high when attribute and constant are equals and operation is 000", output = '1');
			
			int_attribute_value <= 10;
			int_const_value     <= 11;
			operation_type <= "000";
			short_wait;
			should("be low when attribute and constant are not equals and operation is 000", output = '0');
			
			int_attribute_value <= 10;
			int_const_value     <= 11;
			operation_type <= "001";
			short_wait;
			should("be high when attribute and constant are not equals and operation is 001", output = '1');
			
			int_attribute_value <= 10;
			int_const_value     <= 10;
			operation_type <= "001";
			short_wait;
			should("be low when attribute and constant are equals and operation is 001", output = '0');
			
			int_attribute_value <= 10;
			int_const_value     <= 11;
			operation_type <= "010";
			short_wait;
			should("be high when attribute is less than constant and operation is 010", output = '1');
			
			int_attribute_value <= 10;
			int_const_value     <= 10;
			operation_type <= "011";
			short_wait;
			should("be high when attribute is less or equals than constant and operation is 011", output = '1');
			
			int_attribute_value <= 11;
			int_const_value     <= 10;
			operation_type <= "100";
			short_wait;
			should("be high when attribute is greater than constant and operation is 100", output = '1');
			
			int_attribute_value <= 11;
			int_const_value     <= 11;
			operation_type <= "101";
			short_wait;
			should("be high when attribute is greater or equals than constant and operation is 101", output = '1');
			
			int_attribute_value <= 11;
			int_const_value     <= 11;
			operation_type <= "111";
			short_wait;
			should("be low when the operation type is invalid", output = '0');
		end procedure;

	begin
		test_premise;
		wait;
	end process;

end architecture;