#ifndef _ISIMPLEAPPLICATION_H_
#define _ISIMPLEAPPLICATION_H_

#include "IApplication.h"
#include "IGate.h"
#include "IRemoteControl.h"

class SimpleApplication: public Application{
public:
	SimpleApplication(int schedulerStrategy);
	void initRules();
	void initFactBase();
	void initSharedPremises();
	void configureStartApplicationAction();
	void codeApplication();
private:
	Gate* gate;
	RemoteControl* remoteControl;
};

#endif
