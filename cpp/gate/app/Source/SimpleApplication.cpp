#include <stdio.h>
#include "system.h"
#include <stddef.h>
#include <iostream>
#include <unistd.h>
#include "pon_io.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_performance_counter.h"

#include "../Include/ISimpleApplication.h"

using namespace std;

SimpleApplication::SimpleApplication(int schedulerStrategy):Application(schedulerStrategy){
	startApplication();
}

void SimpleApplication::initRules(){
	Condition* cond1 = new Condition(Condition::CONJUNCTION);
	cond1->addPremise(new Premise(remoteControl->bIsPressed,Boolean::TRUE_NOP,Premise::EQUAL)); //(*remoteControl).bIsPressed
	cond1->addPremise(new Premise(gate->GateStatus,2,Premise::EQUAL));
	Action* action1 = new Action();
	Rule* rule1 = new Rule("Open gate", scheduler, cond1, action1, false);
	action1->addInstigation(new Instigation(remoteControl->bIsPressed,false));
	action1->addInstigation(new Instigation(gate->mtOpenGate));

	Condition* cond2 = new Condition(Condition::CONJUNCTION);
	cond2->addPremise(new Premise(remoteControl->bIsPressed,Boolean::TRUE_NOP,Premise::EQUAL));
	cond2->addPremise(new Premise(gate->GateStatus,0,Premise::EQUAL));
	Action* action2 = new Action();
	Rule* rule2 = new Rule("Close gate", scheduler, cond2, action2, false);
	action2->addInstigation(new Instigation(remoteControl->bIsPressed,false));
	action2->addInstigation(new Instigation(gate->mtCloseGate));
}

void SimpleApplication::initFactBase(){
	gate = new Gate();
	remoteControl = new RemoteControl();
}

void SimpleApplication::initSharedPremises(){
}

void SimpleApplication::configureStartApplicationAction(){
}

#include <map>

void SimpleApplication::codeApplication(){



	int c=0;
	while (c!=-1){
		cout << "Gate initial status: " << gate->GateStatus->getValue() << endl;
		cout << "Press button? (1): ";
		cin >> c;
		if (c==1){
			PERF_RESET (PERFORMANCE_COUNTER_0_BASE);
			PERF_START_MEASURING (PERFORMANCE_COUNTER_0_BASE);

			remoteControl->bIsPressed->setValue(true);

//			PERF_STOP_MEASURING (PERFORMANCE_COUNTER_0_BASE);

			long int totalTime = perf_get_total_time(alt_get_performance_counter_base());
			cout << "Ciclos de clock: " << totalTime << endl;

			remoteControl->bIsPressed->setValue(false);

		}
	}
}

//#include "sys/alt_irq.h"

int main() {
	write_red_leds(0x0000);
	write_green_leds(0x0000);
	usleep(500000);

	write_red_leds(0xFF);
	cout << "Iniciando... ";

	map<char,int> hash;
	for (int i=0; i<50; i++) { hash['a'+i] = i; }
	for (int i=0; i<50; i++) { cout << hash['a'+i] << " "; }
	cout << endl;

//	map<char,int>::iterator it;
//    for (it=hash.begin(); it != hash.end(); ++it) {
//        *it = *it + 1;
//    }
//	for (int i=0; i<50; i++) { hash['a'+i] = hash['a'+i] + 1; }
//	cout << "Hello world!!!\n";

	int a, b, c;
	a = 1;
	b = 2;
//	int context = alt_irq_disable_all();

	PERF_RESET (PERFORMANCE_COUNTER_0_BASE);
	PERF_START_MEASURING (PERFORMANCE_COUNTER_0_BASE);
	PERF_BEGIN(PERFORMANCE_COUNTER_0_BASE, 1);
	c = a + b;
	PERF_END(PERFORMANCE_COUNTER_0_BASE, 1);
	PERF_STOP_MEASURING (PERFORMANCE_COUNTER_0_BASE);
	alt_u64 totalTime = perf_get_total_time(alt_get_performance_counter_base());

//	alt_irq_enable_all(context);

//	  perf_print_formatted_report(
//	     (void *)PERFORMANCE_COUNTER_0_BASE, // Peripheral's HW base address
//	      ALT_CPU_FREQ,             // defined in "system.h"
//	      1,                        // How many sections to print
//	      "ts_overhead");

	cout << "Ciclos de clock: " << totalTime << endl;
	cout << c;

	for (int i=0; i<50; i++) { cout << hash['a'+i] << " "; }
	cout << endl;
	 totalTime = perf_get_section_time((void *)PERFORMANCE_COUNTER_0_BASE, 1);
	cout << "Ciclos de clock: " << totalTime << endl;

	while (true) {}

	SimpleApplication simpleApplication(SchedulerStrategy::NO_ONE);
	
	cout << "END";
	write_green_leds(0xFF);
	
	
	return 0;
}

