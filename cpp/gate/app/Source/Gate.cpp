#include "../Include/IGate.h"
#include <iostream>

using namespace std;

/* GateStatus:
0: Open
2: Close */

Gate::Gate(){
	GateStatus = new Integer(this, 2);
	mtOpenGate = new MethodPointer<Gate>(this,&Gate::OpenGate);
	mtCloseGate = new MethodPointer<Gate>(this,&Gate::CloseGate);
}

void Gate::OpenGate(){
	GateStatus->setValue(0);
	cout << "OPEN" << endl;
}

void Gate::CloseGate(){
	GateStatus->setValue(2);
	cout << "CLOSE" << endl;
}
