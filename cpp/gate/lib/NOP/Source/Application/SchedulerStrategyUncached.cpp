#include "ISchedulerStrategyUncached.h"
#include "IRule.h"

SchedulerStrategyUncached::SchedulerStrategyUncached(void):SchedulerStrategy(){
	this->rule = 0;
}

SchedulerStrategyUncached::~SchedulerStrategyUncached(void){
}

void SchedulerStrategyUncached::execute(){
	
  
}

void SchedulerStrategyUncached::addRuleToExecutionList(Rule *rule){
	this->rule = rule;
	
	if(rule->isDerived()){	
		rule->setExecuting(true);
		rule->executeRule();					
		rule->setExecuting(false);
	}
	else{
		rule->setExecuting(true);
		rule->execute();					
		rule->setExecuting(false);
	}	 
}

void SchedulerStrategyUncached::removeRuleToExecutionList(Rule *rule){
}
