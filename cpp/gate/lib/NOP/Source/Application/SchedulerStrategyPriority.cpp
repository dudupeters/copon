#include "ISchedulerStrategyPriority.h"

#include "IRule.h"

SchedulerStrategyPriority::SchedulerStrategyPriority(void) : SchedulerStrategy(){
	this->amountOfRules = 0;
	for(int i = 0; i < 101 ; i++){
		rulesToExecute[i] = 0;
	}
}

SchedulerStrategyPriority::~SchedulerStrategyPriority(void){
}

void SchedulerStrategyPriority::incrementAmountOfRules(){
	this->amountOfRules++;
}

void SchedulerStrategyPriority::decrementAmountOfRules(){
	this->amountOfRules--;
}

int SchedulerStrategyPriority::getAmountOfRules(){
	return amountOfRules;	
}
/**
 * Control the execution list. Extract each Rule of the list and execute it.
 */
void SchedulerStrategyPriority::execute(){
	//int i;	
	//
	//while(getAmountOfRules() > 0){
	//		
	//	for (i = 0; i <= Rule::getAmountOfRules(); i++){
	//		if(rulesToExecute[i] != NULL){
	//			if(rulesToExecute[i]->isDerived()){	
	//			   rulesToExecute[i]->setExecuting(true);
	//			   rulesToExecute[i]->executeRule();
	//			   rulesToExecute[i]->setExecuting(false);
	//			   rulesToExecute[i]->reproved();
	//			}
	//			else{
	//			   rulesToExecute[i]->setExecuting(true);
	//			   rulesToExecute[i]->execute();
	//			   rulesToExecute[i]->setExecuting(false);
	//			   rulesToExecute[i]->reproved();				
	//			}

	//			
	//		}
	//		else{
	//			//std::cout << "Position " << i << " NULL" << std::endl;
	//		}
	//	}	
	//}
}

void SchedulerStrategyPriority::addRuleToExecutionList(Rule *rule){
	//rulesToExecute[rule->getId()] = rule;
	//incrementAmountOfRules();
}

void SchedulerStrategyPriority::removeRuleToExecutionList(Rule *rule){
	//rulesToExecute[rule->getId()] = NULL;
	//decrementAmountOfRules();
}
