#include "ISubCondition.h"

SubCondition::SubCondition(LogicalOperator* logicalOperator, bool exclusive): Condition(logicalOperator){
	setExclusive(exclusive);
}

SubCondition::SubCondition(int operattor, bool exclusive): Condition(operattor){	
	setExclusive(exclusive);
}

SubCondition::~SubCondition(void){
}

void SubCondition::logicCalculus(){
	bool result = logicalOperator->isTrue(amountElements, amountElementsTrue);	
	if(result != this->getLogicValue()){
		setLogicValue(result);
		notifyConditions();
	}	
}

bool SubCondition::calculateLogicalValue(){
	bool result = logicalOperator->isTrue(amountElements, amountElementsTrue);	
	setLogicValue(result);
	return result;
}

void SubCondition::addPremise(Premise* premise){
	Condition::addPremise(premise);
}

void SubCondition::notifyConditions(){
	Condition* tmpCondition = 0; 

	for (it = conditionList.begin(); it != conditionList.end(); ++it){		   
		tmpCondition = ((Condition*)(*it));

		if(getLogicValue() == true){
			tmpCondition->incrementElementsTrue();
		}				
		else{
			tmpCondition->decrementElementsTrue();
		}			

		tmpCondition->logicCalculus();	

		if(getLogicValue()){
			bool conditionLogicValue =  tmpCondition->getLogicValue();
			if(isExclusive() && conditionLogicValue){
				break;
			}
		}	
	}		
}

void SubCondition::addCondition(Condition *condition){
	conditionList.push_back(condition);
}

void SubCondition::setExclusive(bool exclusive){
	this->exclusive = exclusive;
}

bool SubCondition::isExclusive(){
	return exclusive;
}
