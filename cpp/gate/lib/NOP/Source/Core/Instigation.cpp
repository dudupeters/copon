#include "IInstigation.h"
#include "IMethod.h"
#include "IBoolean.h"
#include "IInteger.h"
#include "IDouble.h"

Instigation::Instigation(Method *method){
	this->method = method;
	parameters = NULL;
	isAttribute = false;
}

Instigation::~Instigation(){
}

Instigation::Instigation(Boolean *attribute, bool value){
	isAttribute = true;
	attributeType = BOOLEAN_NOP;
	this->attribute = attribute; 
	this->bValue = value;
	parameters = NULL;
}

Instigation::Instigation(Integer *attribute, int value){
	isAttribute = true;
	attributeType = INTEGER_NOP;
	this->attribute = attribute;
	this->iValue = value;
	parameters = NULL;
}

Instigation::Instigation(Double *attribute, double value){
	attributeType = DOUBLE_NOP;
	isAttribute = true;
	this->attribute = attribute;
	this->dValue = value;
	parameters = NULL;
}

Instigation::Instigation(Char *attribute, char value){
	attributeType = CHAR_NOP;
	isAttribute = true;
	this->attribute = attribute;
	this->cValue = value;
	parameters = NULL;
}

Instigation::Instigation(String *attribute, char* value){
	attributeType = STRING_NOP;
	isAttribute = true;
	this->attribute = attribute;
	this->sValue = value;
	parameters = NULL;
}

Instigation::Instigation(Method* method, MyList< Attribute > *attributes ){
	isAttribute = false; 
	this->method = method;
	parameters = attributes;	
}

void Instigation::execute(){
	
	if(isAttribute){
		switch(attributeType){
			case BOOLEAN_NOP: 
				((Boolean*)attribute)->setValue(bValue);				
				break;
			case INTEGER_NOP:
				((Integer*)attribute)->setValue(iValue); 
				break;
			case DOUBLE_NOP:
				((Double*)attribute)->setValue(dValue); 
				break;
			case CHAR_NOP:
				((Char*)attribute)->setValue(cValue); 
				break;
			case STRING_NOP:
				((String*)attribute)->setValue(sValue); 
				break;
			default:
				break;		
		}		
	}
	else if(parameters != NULL){			
		    method->execute(parameters);		 
		 }
		 else{
			 method->execute();
		 }	
}
