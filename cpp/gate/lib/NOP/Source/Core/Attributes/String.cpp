#include "IString.h"

String::String(){
	this->myString = new char[40];
}

String::String(char* value) : Attribute((FBE*)NULL){
	this->myString = (char*)value;
}

String::String(FBE* fact, char* value) : Attribute(fact){
	this->myString = (char*)value;
}

String::~String(void){

}

char* String::getValue(){
	return myString;
}

void String::setValue(char* value){
	if(strcmp(myString, value) != 0){
		myString = new char[40];
		strcpy(myString, value);
		notifyPremises();
	}
		
}

void String::setValue(Attribute *value){
	setValue(((String*)value)->getValue());
}

void String::setValue(char* value, int flag){
	
	if (strcmp(this->myString, value) != 0){
		strcpy(this->myString, value);
		if (flag != Attribute::NO_NOTIFY)
			notifyPremises();
	}
	else if (flag == Attribute::RENOTIFY)
		renotifyPremises();
	
}

void String::setValue(Attribute *value, int flag){
    setValue( ( (String*) value)->getValue(), flag);
}

bool String::isValueChange(Attribute *value){
	if( strcmp( myString, ((String*)value)->getValue()) != 0 )
		return true;
		return false;			
}

bool String::isEqual(Attribute *attributeB){
	if( strcmp( myString, ((String*)attributeB)->getValue()) == 0 )
		return true;
		return false;		
}

bool String::isGreaterThan(Attribute *attributeB){
	if( strcmp( myString, ((String*)attributeB)->getValue()) > 0 )
		return true;
		return false;
}

bool String::isSmallerThan(Attribute *attributeB){
	if( strcmp( myString, ((String*)attributeB)->getValue()) < 0 )
		return false;		
	    return true;		
}

bool String::isGreaterOrEqualThan(Attribute *attributeB){
	if( strcmp( myString, ((String*)attributeB)->getValue()) >= 0 )
		return true;
		return false;
}

bool String::isSmallerOrEqualThan(Attribute *attributeB){
	if( strcmp( myString, ((String*)attributeB)->getValue()) <= 0 )
		return true;
		return false;
}

bool String::isDifferent(Attribute *attributeB){
	if( strcmp( myString, ((String*)attributeB)->getValue()) != 0 )
		return true;
	    return false;
}
