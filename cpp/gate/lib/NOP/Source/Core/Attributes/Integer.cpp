#include "IInteger.h"

/**
* Integer default constructor
*/
Integer::Integer(){
	this->myInteger = 0;
}

/**
* Integer constructor
* @param fact reference to the FBE of the Attribute Integer
* @param value a initial Integer value
*/
Integer::Integer(FBE* fact, int value): Attribute(fact){
	this->myInteger = value;
}

/**
* Integer constructor
* @param value a initial Integer value
*/
Integer::Integer(int value): Attribute((FBE*)NULL){
	this->myInteger = value;
}

/**
* Integer constructor
* @param address to a Integer value
*/
Integer::Integer(Integer& attr): Attribute(&attr) {
    this->myInteger = attr.myInteger;
}

/**
* Integer default destructor
*/
Integer::~Integer(void){
}

/**
* Returns the Integer primitive value
* @param myChar a Integer primitive value/state
*/
int Integer::getValue(){
	return myInteger;
} 

/**
* Update the Integer Attribute value. 
* This method start a notification flow if the assigned value is different of the last value.
* @param value a primitive Integer value to be assigned
*/
void Integer::setValue(int value){
	if(this->myInteger != value){
	     this->myInteger = value;
	     notifyPremises();	
	}
}

/**
* Update the Integer Attribute value.
* This method start a notification flow if the assigned value is different of the last value.
* @param value a Integer object
*/
void Integer::setValue(Attribute *value){
	
	setValue(((Integer*)value)->getValue());
	//myInteger = ((Integer*)value)->getValue();
	//notifyPremises();	
}

/**
* Update the Integer Attribute value.
* This method start a notification flow if the assigned value is different or even the same than the last one.
* This method is used when the renotification mechanism is desired.
* @param value a primitive Integer value to be assigned
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void Integer::setValue(int value, int flag){
	
	if (this->myInteger != value){
		this->myInteger = value;
		if (flag != Attribute::NO_NOTIFY)
			notifyPremises();
	}
	else if (flag == Attribute::RENOTIFY)
		renotifyPremises();
	
}

/**
* Update the Integer Attribute value.
* This method start a notification flow if the assigned value is different or even the same than the last one.
* This method is used when the renotification mechanism is desired.
* @param value a Integer value to be assigned
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void Integer::setValue(Attribute *value, int flag){
	setValue( ( (Integer*) value)->getValue(), flag);
}

/**
* Verify if the actual value is changed.
* @param value assigned object value
* @return the result of the comparison
*/
/*bool Integer::isValueChange(Integer *value){
	if(myInteger != (value->getValue()))	
		return true;
		return false;  
}*/

/**
* Verify if the assigned value(object) is equal to the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Integer::isEqual(Attribute *attributeB){
	if( myInteger == ((Integer*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is greater than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Integer::isGreaterThan(Attribute *attributeB){
	if( myInteger > ((Integer*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is smaller than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Integer::isSmallerThan(Attribute *attributeB){
	if( myInteger < ((Integer*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is greater or equal than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Integer::isGreaterOrEqualThan(Attribute *attributeB){
	if( myInteger >= ((Integer*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is smaller than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Integer::isSmallerOrEqualThan(Attribute *attributeB){
	if( myInteger <= ((Integer*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is different than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Integer::isDifferent(Attribute *attributeB){
	if( myInteger != ((Integer*)attributeB)->getValue())
		return true;
		return false;
}
