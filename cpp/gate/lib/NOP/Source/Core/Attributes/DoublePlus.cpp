#include "IDoublePlus.h"

/**
* DoublePlus default constructor
*/
DoublePlus::DoublePlus() : Double() {}

/**
* DoublePlus constructor
* @param value a initial Double value
*/
DoublePlus::DoublePlus(double value): Double(value) {}

/**
* DoublePlus constructor
* @param fact reference to the FBE of the Attribute Double
* @param value a initial Double value
*/
DoublePlus::DoublePlus(FBE* fact, double value): Double(fact, value) {}

/**
* DoublePlus constructor
* @param address to a Double value
*/
DoublePlus::DoublePlus(Double& attr): Double(attr) {}

/**
* DoublePlus default destructor
*/
DoublePlus::~DoublePlus(void) {}

/**
* Adds a value to the current value
* @param value to be added
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void DoublePlus::add(Double* value, int flag){
	setValue( ( myDouble + value->getValue() ), flag );
}

/**
* Adds a value to the current value
* @param value to be added
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void DoublePlus::add(double value, int flag){
	setValue( ( myDouble + value ), flag );
}

/**
* Subtracts a value to the current value
* @param value to be subtracted
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void DoublePlus::subtract(Double* value, int flag){
	setValue( ( myDouble - value->getValue() ), flag );
}

/**
* Subtracts a value to the current value
* @param value to be subtracted
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void DoublePlus::subtract(double value, int flag){
	setValue( ( myDouble - value ), flag );
}

/**
* Multiplies a value to the current value
* @param value to be multiplied
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void DoublePlus::multiply(Double* value, int flag){
	setValue( ( myDouble * value->getValue() ), flag );
}

/**
* Multiplies a value to the current value
* @param value to be multiplied
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void DoublePlus::multiply(double value, int flag){
	setValue( ( myDouble * value ), flag );
}

/**
* Divides a value to the current value
* @param value to be divided
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void DoublePlus::divide(Double* value, int flag){
	double tmp = value->getValue();
	if ( tmp == 0.0 )
		setValue( 0.0, flag );
	else
		setValue( ( myDouble / tmp ), flag );
}

/**
* Divides a value to the current value
* @param value to be divided
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void DoublePlus::divide(double value, int flag){
	if ( value == 0.0 )
		setValue( 0.0, flag );
	else
		setValue( ( myDouble / value ), flag );
}
