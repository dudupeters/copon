#include "IDouble.h"

/**
* Double default constructor
*/
Double::Double() {
	this->myDouble = 0.0;
}

/**
* Double constructor
* @param value a initial Double value
*/
Double::Double(double value): Attribute((FBE*)NULL){
	this->myDouble = value;
}

/**
* Double constructor
* @param fact reference to the FBE of the Attribute Double
* @param value a initial Double value
*/
Double::Double(FBE* fact, double value): Attribute(fact){
	this->myDouble = value;
}

/**
* Double constructor
* @param address to a Double value
*/
Double::Double(Double& attr): Attribute(&attr) {
    this->myDouble = attr.myDouble;
}

/**
* Double default destructor
*/
Double::~Double(void){
}

/**
* Returns the Double primitive value
* @param myChar a Double primitive value/state
*/
double Double::getValue(){
	return myDouble;
} 

/**
* Update the Double Attribute value. 
* This method start a notification flow if the assigned value is different of the last value.
* @param value a primitive Double value to be assigned
*/
void Double::setValue(double value){
	if(this->myDouble != value){
		this->myDouble = value;
		notifyPremises();	
	}
	
	
}

/**
* Update the Double Attribute value.
* This method start a notification flow if the assigned value is different of the last value.
* @param value a Double object
*/
void Double::setValue(Attribute *value){
	setValue(((Double*)value)->getValue());
}

/**
* Update the Double Attribute value.
* This method start a notification flow if the assigned value is different or even the same than the last one.
* This method is used when the renotification mechanism is desired.
* @param value a primitive Double value to be assigned
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void Double::setValue(double value, int flag){
	
	if (this->myDouble != value){
		this->myDouble = value;
		if (flag != Attribute::NO_NOTIFY)
			notifyPremises();
	}
	else if (flag == Attribute::RENOTIFY)
		renotifyPremises();
	
}

/**
* Update the Double Attribute value.
* This method start a notification flow if the assigned value is different or even the same than the last one.
* This method is used when the renotification mechanism is desired.
* @param value a Double value to be assigned
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void Double::setValue(Attribute *value, int flag){
    setValue( ( (Double*) value)->getValue(), flag);
}

/**
* Verify if the assigned value(object) is equal to the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Double::isEqual(Attribute *attributeB){
	if( myDouble == ((Double*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is greater than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Double::isGreaterThan(Attribute *attributeB){
	if( myDouble > ((Double*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is smaller than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Double::isSmallerThan(Attribute *attributeB){
	if( myDouble < ((Double*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is greater or equal than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Double::isGreaterOrEqualThan(Attribute *attributeB){
	if( myDouble >= ((Double*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is smaller than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Double::isSmallerOrEqualThan(Attribute *attributeB){
	if( myDouble <= ((Double*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is different than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Double::isDifferent(Attribute *attributeB){
	if( myDouble != ((Double*)attributeB)->getValue())
		return true;
		return false;
}
