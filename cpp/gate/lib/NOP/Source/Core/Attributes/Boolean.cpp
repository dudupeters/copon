#include "IBoolean.h"

Boolean* Boolean::TRUE_NOP = new Boolean(NULL, true);
Boolean* Boolean::FALSE_NOP = new Boolean(NULL, false);

/**
* Boolean default constructor
*/
Boolean::Boolean():Attribute(){
	this->myBool = false;	
}

/**
* Boolean constructor
* @param value a initial Boolean value
*/
Boolean::Boolean(bool value):Attribute(){
	this->myBool = value;	
}

/**
* Boolean constructor
* @param fact reference to the FBE of the Attribute Boolean
* @param value a initial Boolean value
*/
Boolean::Boolean(FBE* fact, bool value):Attribute(fact){
	this->myBool = value;	
}

/**
* Boolean default destructor
*/
Boolean::~Boolean(){
}

/**
* Returns the Boolean primitive value
* @param myBool Boolean primitive value/state
*/
bool Boolean::getValue(){
	return this->myBool;	
}

/**
* Update the Boolean Attribute value. 
* This method start a notification flow if the assigned value is different of the last value.
* @param value a primitive Boolean value to be assigned
*/
void Boolean::setValue(bool value){	
	if(this->myBool != value){
		this->myBool = value;
		notifyPremises();
	}	
}

/**
* Update the Boolean Attribute value.
* This method start a notification flow if the assigned value is different or even the same than the last one.
* This method is used when the renotification mechanism is desired.
* @param value a primitive Boolean value to be assigned
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void Boolean::setValue(bool value, int flag){
	
	if (this->myBool != value){
		this->myBool = value;
		if (flag != Attribute::NO_NOTIFY)
			notifyPremises();
	}
	else if (flag == Attribute::RENOTIFY)
		renotifyPremises();
	
}

/**
* Update the Boolean Attribute value.
* This method start a notification flow if the assigned value is different of the last value.
* @param value a Boolean object
*/
void Boolean::setValue(Attribute *value){
	setValue(((Boolean*)value)->getValue());
}

/**
* Verify if the assigned value(object) is equal to the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Boolean::isEqual(Attribute* attributeB){
	if( myBool == ((Boolean*)attributeB)->getValue())
		return true;
		return false;
}

/**
* No applied to the Boolean class.
* @param attributeB a object value to be compared
* @return always return false.
*/
bool Boolean::isGreaterThan(Attribute* attributeB){
	return false;	
}

/**
* No applied to the Boolean class.
* @param attributeB a object value to be compared
* @return always return false.
*/
bool Boolean::isSmallerThan(Attribute *attributeB){
	return false;
}

/**
* No applied to the Boolean class.
* @param attributeB a object value to be compared
* @return always return false.
*/
bool Boolean::isGreaterOrEqualThan(Attribute *attributeB){
	return false;
}

/**
* No applied to the Boolean class.
* @param attributeB a object value to be compared
* @return always return false.
*/
bool Boolean::isSmallerOrEqualThan(Attribute *attributeB){
	return false;
}

/**
* Verify if the assigned value(object) is different to the actual value(object).
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Boolean::isDifferent(Attribute *attributeB){
	if( myBool != ((Boolean*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the actual value is changed.
* @param value assigned object value
* @return the result of the comparison
*/
