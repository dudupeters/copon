#include "IRuleObject.h"
#include "IRule.h"

RuleObject::RuleObject(char* name,  Scheduler* scheduler, int logicalOperator) : Rule(name, scheduler, new Condition(logicalOperator)){
	
	dependentRulesList = new MyList<RuleObject>();

	masterRuleExecuted = new FBE();
	atMasterRuleExecuted = new Boolean( masterRuleExecuted, false );

}

RuleObject::~RuleObject(){
	
}

void RuleObject::addPremise(Attribute* attributeA, Attribute* attributeB, int operattor, bool exclusive, int attributesToSave){
	Premise* premiseTmp = new Premise(attributeA, attributeB, operattor, exclusive); 
	this->condition->addPremise(premiseTmp);
	
	switch(attributesToSave){
		case Premise::NO_ONE:
			break;		
		case Premise::FIRST_ONLY:
			//this->attributes[attributeA->getName()] = attributeA;
			//this->attributes[1] = attributeA;
			this->attributes.push_back(attributeA);
			break;
		case Premise::SECOND_ONLY:
			//this->attributes[attributeB->getName()] = attributeB;
			//this->attributes[2] = attributeB;
			this->attributes.push_back(attributeB);
			break;		
		case Premise::BOTH:
			//this->attributes[1] = attributeA;
			//this->attributes[2] = attributeB;
			this->attributes.push_back(attributeA);
			this->attributes.push_back(attributeB);
			break;
	}
}

void RuleObject::addPremise(char *name, Attribute *attributeA, Attribute *attributeB, int operattor, bool exclusive, int attributesToSave){
	condition->addPremise(new Premise(name, attributeA, attributeB, operattor, exclusive));
	
	switch(attributesToSave){
		case Premise::NO_ONE:
			break;		
		case Premise::FIRST_ONLY:
			//this->attributes[1] = attributeA;
			this->attributes.push_back(attributeA);
			break;
		case Premise::SECOND_ONLY:
			//this->attributes[2] = attributeB;
			this->attributes.push_back(attributeB);
			break;		
		case Premise::BOTH:
			//this->attributes[1] = attributeA;
			//this->attributes[2] = attributeB;
			this->attributes.push_back(attributeA);
			this->attributes.push_back(attributeB);
			break;
	}
}

void RuleObject::addPremise(Attribute *attributeA, bool atBoolean, int operattor, bool exclusive, int attributesToSave){
	condition->addPremise(new Premise(attributeA, atBoolean, operattor, exclusive));
	
	switch(attributesToSave){
		case Premise::NO_ONE:
			break;		
		case Premise::FIRST_ONLY:
			//this->attributes[1] = attributeA;
			this->attributes.push_back(attributeA);
			break;
	}
}

void RuleObject::addPremise(Attribute *attributeA, int atInteger, int operattor, bool exclusive, int attributesToSave){
	condition->addPremise(new Premise(attributeA, atInteger, operattor, exclusive));
	
	switch(attributesToSave){
		case Premise::NO_ONE:
			break;		
		case Premise::FIRST_ONLY:
			//this->attributes[1] = attributeA;
			this->attributes.push_back(attributeA);
			break;
	}
}

void RuleObject::addPremise(Attribute *attributeA, double atDouble, int operattor, bool exclusive, int attributesToSave){
	condition->addPremise(new Premise(attributeA, atDouble, operattor, exclusive));
	
	switch(attributesToSave){
		case Premise::NO_ONE:
			break;		
		case Premise::FIRST_ONLY:
			//this->attributes[1] = attributeA;
			this->attributes.push_back(attributeA);
			break;
	}
}

void RuleObject::addPremise(Attribute *attributeA, char atChar, int operattor, bool exclusive, int attributesToSave){
	condition->addPremise(new Premise(attributeA, atChar, operattor, exclusive));
	
	switch(attributesToSave){
		case Premise::NO_ONE:
			break;		
		case Premise::FIRST_ONLY:
			//this->attributes[1] = attributeA;
			this->attributes.push_back(attributeA);
			break;
	}
}

void RuleObject::addPremise(Attribute *attributeA, char* atString, int operattor, bool exclusive, int attributesToSave){
	condition->addPremise(new Premise(attributeA, atString, operattor, exclusive));
	
	switch(attributesToSave){
		case Premise::NO_ONE:
			break;		
		case Premise::FIRST_ONLY:
			//this->attributes[1] = attributeA;
			this->attributes.push_back(attributeA);
			break;
	}
}

void RuleObject::addPremise(Premise *premise){
	condition->addPremise(premise);
}

void RuleObject::addInstigation(Instigation *instigation){
	action->addInstigation(instigation);
}

void RuleObject::addInstigation(Boolean *attribute, bool bValue){
	action->addInstigation(new Instigation(attribute, bValue));
}

void RuleObject::addInstigation(Integer *attribute, int iValue){
	action->addInstigation(new Instigation(attribute, iValue));
}

void RuleObject::addInstigation(Double *attribute, double dValue){
	action->addInstigation(new Instigation(attribute, dValue));
}

void RuleObject::addInstigation(Char *attribute, char cValue){
	action->addInstigation(new Instigation(attribute, cValue));
}

void RuleObject::addInstigation(String *attribute, char* sValue){
	action->addInstigation(new Instigation(attribute, sValue));
}

//Passa a lista de attributes como parametro correspondentes aos attributes declarados nas premises)
//Usado para chamada de Method
void RuleObject::addMethodWithParameters(Method *method){
	action->addInstigation(new Instigation(method, &attributes));
}

void RuleObject::addMethod(Method *method){
	action->addInstigation(new Instigation(method));
}

void RuleObject::addSubCondition(int logicalOperator, bool exclusive){
	subCondition = new SubCondition(logicalOperator, exclusive);
	condition->addSubCondition(subCondition);
}

//void RuleObject::addSubCondition(LogicalOperator logicalOperator, bool exclusive){
//	subCondition = new SubCondition(logicalOperator, exclusive);
//	condition->addSubCondition(subCondition);
//}

void RuleObject::addPremiseToSubCondition(Attribute *attributeA, Attribute *attributeB, int operattor, bool exclusive){
	subCondition->addPremise(new Premise(attributeA, attributeB, operattor, exclusive));
}

void RuleObject::addPremiseToSubCondition(Attribute *attributeA, bool atBoolean, int operattor, bool exclusive){
	subCondition->addPremise(new Premise(attributeA, atBoolean, operattor, exclusive));
}

void RuleObject::addPremiseToSubCondition(Attribute *attributeA, int atInteger, int operattor, bool exclusive){
	subCondition->addPremise(new Premise(attributeA, atInteger, operattor, exclusive));
}

void RuleObject::addPremiseToSubCondition(Attribute *attributeA, double atDouble, int operattor, bool exclusive){
	subCondition->addPremise(new Premise(attributeA, atDouble, operattor, exclusive));
}

void RuleObject::addPremiseToSubCondition(Premise *premise){
	subCondition->addPremise(premise);
}

void RuleObject::execute(){
	if (isApproved()){ 
		action->execute();
		atMasterRuleExecuted->setValue( Boolean::FALSE_NOP );
		notifyDependentRules(); // Notifica as regras dependentes que a regra master foi executada.
    }
}

void RuleObject::addMasterRule( RuleObject* masterRule ){
	masterRule->dependentRulesList->push_back( this );
	this->addSubCondition( Condition::SINGLE, false );
	this->addPremiseToSubCondition( new Premise( this->atMasterRuleExecuted, Boolean::TRUE_NOP, Premise::EQUAL ) );
}

void RuleObject::notifyDependentRules(){
	//for ( int i = 0; i < this->dependentRulesList->getSize(); i++ )
	while (dependentRulesList->hasNext())
		//this->dependentRulesList->getElement( i )->atMasterRuleExecuted->setValue( Boolean::TRUE_NOP );
		dependentRulesList->Next()->atMasterRuleExecuted->setValue(Boolean::TRUE_NOP );
}
