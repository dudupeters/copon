#include "IRule.h"

int Rule::amountOfRules = 0;

Rule::Rule(char* name, Scheduler* scheduler, Condition* condition, Action* action, bool IsDerived){
	this->name = name;		
	this->condition = condition;
	this->action = action;
	condition->setRule(this);
	action->setRule(this);
	this->scheduler = scheduler;

    // Modificado por Fernando Witt 05-02-2010
	//isDerivedd = false;
	//this->condition->logicCalculus();
	isDerivedd = IsDerived;
	if ( !isDerivedd )
		this->condition->logicCalculus();
	id = amountOfRules++;

}

Rule::Rule(char* name, Scheduler* scheduler, Condition* condition){
	this->name = name;		
	this->condition = condition;
	//this->condition->logicCalculus();
	this->action = new Action();
	condition->setRule(this);
	action->setRule(this);
	this->scheduler = scheduler;
	this->isDerivedd = false;
	id = amountOfRules++;

}

Rule::Rule(char* name, Scheduler* scheduler, Condition* condition, Action* action, int priority){
	this->name = name;		
	this->condition = condition;
	this->action = action;
	condition->setRule(this);
	action->setRule(this);
	this->scheduler = scheduler;
	this->priority = priority;
	this->isDerivedd = false;

	this->condition->logicCalculus();
	id = amountOfRules++;

}

Rule::~Rule(void){
	delete action;       
    delete condition;
}

#include "altera_avalon_performance_counter.h"
#include "system.h"

bool Rule::approved(){

	PERF_STOP_MEASURING (PERFORMANCE_COUNTER_0_BASE);

	if(scheduler->getType() == SchedulerStrategy::NO_ONE){
		setApproved(true);	
		//setExecuting(true);

		// Modificado por Fernando Augusto de Witt  05-02-2010
		// Se a Regra for derivada, ent�o o metodo chamado � o executeRule
		// sen�o � chamado normalmente o execute.
		if ( isDerivedd )
			executeRule();
		else{
			setExecuting(true);
			execute();
			setExecuting(false);			
			setApproved(false);	  
		}

		//setExecuting(false);			
		//setApproved(false);	
	}
	else{
   //if (!isExecuting()){
	  setApproved(true);
	  scheduler->approveRule(this);		
   //}
	}
   return true;
   //return false;
}

bool Rule::reproved(){
	
	if (!isExecuting()){
	  if (isApproved()){
		  // Essa Condi��o foi adicionada por Fernando Witt 09-02-2010
		  // Objetivo: Evitar que removeApprovedRule, do scheduler,
		  // fosse chamado mesmo usando a estrat�gia NO_ONE
		if(!(scheduler->getType() == SchedulerStrategy::NO_ONE))
			scheduler->removeApprovedRule(this);
		setExecuting(false);
		setApproved(false);
		return true;
	  }
	}	
	return false;
}

void Rule::execute(){
	if (isApproved()){ 
		action->execute();
    }
}

bool Rule::isExecuted(){
	return action->isExecuted();
}

bool Rule::isExecuting(){
	return executing;
}

bool Rule::isApproved(){
	return approvedd;
}

int Rule::getPriority(){
	return priority;
}

void Rule::setExecuting(bool executing){
	this->executing = executing;
}

void Rule::setApproved(bool approvedd){
	this->approvedd = approvedd;
}

void Rule::setPriority(int priority){
	this->priority = priority;
}

void Rule::setStart(bool init){
	start = init;
}

int Rule::getAmountOfRules(){
	return amountOfRules;		   
}

int Rule::getId(){
	return id;
}

bool Rule::isStart(){
	return this->start;
}

bool Rule::isDerived(){
	return this->isDerivedd;
}
void Rule::end(){
	condition->logicCalculus();
}


void Rule::executeRule(){
	
}

Condition* Rule::getCondition(){
	return this->condition;
}
