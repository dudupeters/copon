#include "IRuleDerived.h"
#include "IRule.h"


RuleDerived::RuleDerived(char* name,  Scheduler* scheduler, int logicalOperator) : Rule(name, scheduler, new Condition(logicalOperator)){
	attributes = new MyList<Attribute>();

}

RuleDerived::~RuleDerived(){
	
}

void RuleDerived::addPremise(Attribute* attributeA, Attribute* attributeB, int operattor, bool exclusive, int attributesToSave){
	Premise* premiseTmp = new Premise(attributeA, attributeB, operattor, exclusive); 
	this->condition->addPremise(premiseTmp);
	
	switch(attributesToSave){
		case Premise::NO_ONE:
			break;		
		case Premise::FIRST_ONLY:
			//this->attributes[1] = attributeA;
			this->attributes->push_back(attributeA);
			break;
		case Premise::SECOND_ONLY:
			//this->attributes[2] = attributeB;
			this->attributes->push_back(attributeB);
			break;		
		case Premise::BOTH:
			//this->attributes[1] = attributeA;
			//this->attributes[2] = attributeB;
			this->attributes->push_back(attributeA);
			this->attributes->push_back(attributeB);
			break;
	}
}

void RuleDerived::addPremise(Attribute *attributeA, bool atBoolean, int operattor, bool exclusive, int attributesToSave){
	condition->addPremise(new Premise(attributeA, atBoolean, operattor, exclusive));
	
	switch(attributesToSave){
		case Premise::NO_ONE:
			break;		
		case Premise::FIRST_ONLY:
			//this->attributes[1] = attributeA;
			this->attributes->push_back(attributeA);
			break;
	}
}

void RuleDerived::addPremise(Attribute *attributeA, int atInteger, int operattor, bool exclusive, int attributesToSave){
	condition->addPremise(new Premise(attributeA, atInteger, operattor, exclusive));
	
	switch(attributesToSave){
		case Premise::NO_ONE:
			break;		
		case Premise::FIRST_ONLY:
			//this->attributes[1] = attributeA;
			this->attributes->push_back(attributeA);
			break;
	}
}

void RuleDerived::addPremise(Attribute *attributeA, double atDouble, int operattor, bool exclusive, int attributesToSave){
	condition->addPremise(new Premise(attributeA, atDouble, operattor, exclusive));
	
	switch(attributesToSave){
		case Premise::NO_ONE:
			break;		
		case Premise::FIRST_ONLY:
			//this->attributes[1] = attributeA;
			this->attributes->push_back(attributeA);
			break;
	}
}

void RuleDerived::addPremise(Premise *premise){
	condition->addPremise(premise);
}

void RuleDerived::addSubCondition(int logicalOperator, bool exclusive){
	subCondition = new SubCondition(logicalOperator, exclusive);
	condition->addSubCondition(subCondition);
}

void RuleDerived::addPremiseToSubCondition(Attribute *attributeA, Attribute *attributeB, int operattor, bool exclusive){
	subCondition->addPremise(new Premise(attributeA, attributeB, operattor, exclusive));
}

void RuleDerived::addPremiseToSubCondition(Attribute *attributeA, bool atBoolean, int operattor, bool exclusive){
	subCondition->addPremise(new Premise(attributeA, atBoolean, operattor, exclusive));
}

void RuleDerived::addPremiseToSubCondition(Attribute *attributeA, int atInteger, int operattor, bool exclusive){
	subCondition->addPremise(new Premise(attributeA, atInteger, operattor, exclusive));
}

void RuleDerived::addPremiseToSubCondition(Attribute *attributeA, double atDouble, int operattor, bool exclusive){
	subCondition->addPremise(new Premise(attributeA, atDouble, operattor, exclusive));
}
