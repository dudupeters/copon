#include "IScheduler.h"

Scheduler::Scheduler(){
	strategy = new SchedulerStrategyBreadth(); //DEFAULT
}

Scheduler::Scheduler(int strategyId){
	switch(strategyId){
	case SchedulerStrategy::BREADTH:
		strategy = new SchedulerStrategyBreadth();		
		type = SchedulerStrategy::BREADTH;
		break;
	case SchedulerStrategy::PRIORITY:
		strategy = new SchedulerStrategyPriority();
		type = SchedulerStrategy::PRIORITY;
		break;
	case SchedulerStrategy::DEPTH:
		strategy = new SchedulerStrategyDepth();
		type = SchedulerStrategy::DEPTH;
		break;
	case SchedulerStrategy::UNCACHED:
		strategy = new SchedulerStrategyUncached();
		type = SchedulerStrategy::UNCACHED;
		break;
	case SchedulerStrategy::NO_ONE:
		strategy = NULL;
		type = SchedulerStrategy::NO_ONE;
		break;
	}	
}

Scheduler::~Scheduler(void){
}

void Scheduler::approveRule(Rule *rule){
	strategy->addRuleToExecutionList(rule);
}

void Scheduler::removeApprovedRule(Rule *rule){
	strategy->removeRuleToExecutionList(rule);
}

bool Scheduler::hasRuleInExecution(){
	return ruleInExecution;
}

void Scheduler::execute(){	
	if(strategy != NULL)
	strategy->execute();
}

int Scheduler::getType(){
	return type;
}

SchedulerStrategy* Scheduler::getStrategy(){
	return strategy;
}