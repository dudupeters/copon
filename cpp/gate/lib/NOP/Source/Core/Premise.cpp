#include "IPremise.h"
#include "IBoolean.h"
#include "IInteger.h"
#include "IDouble.h"

Premise::Premise(char* name, Attribute* attributeA, Attribute* attributeB, int operattor, bool exclusive){
	this->attributeA = attributeA;
	this->attributeB = attributeB;
	this->operattor = operattor;
	this->exclusive = exclusive;
	setInit(true);
	setLogicValue( this->logicCalculus() ); // Calcula o estado l�gico da premissa no momento de sua cria��o.
	this->attributeA->addPremise(this);
	this->attributeB->addPremise(this);	
	setActive(true);	
	setName(name);
    conditionExclusive = 0;
	conditionList = new MyList<Condition>();
	numOfConditions = 0;
}

Premise::Premise(Attribute* attributeA, Attribute* attributeB, int operattor, bool exclusive){
	this->attributeA = attributeA;
	this->attributeB = attributeB;
	this->operattor = operattor;
	this->exclusive = exclusive;
	setInit(true);
	setLogicValue( this->logicCalculus() ); // Calcula o estado l�gico da premissa no momento de sua cria��o.
	this->attributeA->addPremise(this);
	this->attributeB->addPremise(this);	
	setActive(true);	
	conditionExclusive = 0;	
	conditionList = new MyList<Condition>();
	numOfConditions = 0;
}

Premise::Premise(Attribute* attributeA, bool atBoolean, int operattor, bool exclusive){
	this->attributeB = new Boolean(NULL, atBoolean);
	this->attributeA = attributeA;	
	this->operattor = operattor;
	this->exclusive = exclusive;
	setInit(true);
	setLogicValue( this->logicCalculus() ); // Calcula o estado l�gico da premissa no momento de sua cria��o.
	this->attributeA->addPremise(this);
	//this->attributeB->addPremise(this); // Comentado, esta linha � desnecess�ria (valor est�tico).
	setActive(true);	
	conditionExclusive = 0;
	conditionList = new MyList<Condition>();
	numOfConditions = 0;
}

Premise::Premise(Attribute* attributeA, int atInteger, int operattor, bool exclusive){
	this->attributeB = new Integer(NULL, atInteger);
	this->attributeA = attributeA;	
	this->operattor = operattor;
	this->exclusive = exclusive;
	setInit(true);
	setLogicValue( this->logicCalculus() ); // Calcula o estado l�gico da premissa no momento de sua cria��o.
	this->attributeA->addPremise(this);	
	//this->attributeB->addPremise(this); // Comentado, esta linha � desnecess�ria.	
	setActive(true);	
	conditionExclusive = 0;
	conditionList = new MyList<Condition>();
	numOfConditions = 0;
}

Premise::Premise(Attribute* attributeA, double atDouble, int operattor, bool exclusive){
	this->attributeB = new Double(NULL, atDouble);
	this->attributeA = attributeA;	
	this->operattor = operattor;
	this->exclusive = exclusive;
	setInit(true);
	setLogicValue( this->logicCalculus() ); // Calcula o estado l�gico da premissa no momento de sua cria��o.
	this->attributeA->addPremise(this);
	//this->attributeB->addPremise(this); // Comentado, esta linha � desnecess�ria.
	setActive(true);	
	conditionExclusive = 0;	
	conditionList = new MyList<Condition>();
	numOfConditions = 0;
}

Premise::Premise(Attribute* attributeA, char atChar, int operattor, bool exclusive){
	this->attributeB = new Char(NULL, atChar);
	this->attributeA = attributeA;	
	this->operattor = operattor;
	this->exclusive = exclusive;
	setInit(true);
	setLogicValue( this->logicCalculus() ); // Calcula o estado l�gico da premissa no momento de sua cria��o.
	this->attributeA->addPremise(this);
	//this->attributeB->addPremise(this); // Comentado, esta linha � desnecess�ria.
	setActive(true);	
	conditionExclusive = 0;	
	conditionList = new MyList<Condition>();
	numOfConditions = 0;
}

Premise::Premise(Attribute* attributeA, char* atString, int operattor, bool exclusive){
	this->attributeB = new String(NULL, atString);
	this->attributeA = attributeA;	
	this->operattor = operattor;
	this->exclusive = exclusive;
	setInit(true);
	setLogicValue( this->logicCalculus() ); // Calcula o estado l�gico da premissa no momento de sua cria��o.
	this->attributeA->addPremise(this);
	//this->attributeB->addPremise(this); // Comentado, esta linha � desnecess�ria.
	setActive(true);	
	conditionExclusive = 0;	
	conditionList = new MyList<Condition>();
	numOfConditions = 0;
}

Premise::~Premise() {  
  this->attributeA->removePremise(this);
  this->attributeB->removePremise(this);  
}

bool Premise::changeValue(bool value){
	if(this->getLogicValue() == value)
		return false;
		return true;	
}

bool Premise::logicCalculus(){
	bool valueLogic = false;
	switch(operattor){
		case EQUAL:	
			valueLogic = attributeA->isEqual(attributeB);
			break;
		case DIFFERENT:
			valueLogic =  attributeA->isDifferent(attributeB);
			break;
		case GREATERTHAN:
			valueLogic =  attributeA->isGreaterThan(attributeB);
			break;
		case SMALLERTHAN:
			valueLogic =  attributeA->isSmallerThan(attributeB);
			break;
		case GREATEROREQUAL:
			valueLogic =  attributeA->isGreaterOrEqualThan(attributeB);
			break;
		case SMALLEROREQUAL:
			valueLogic =  attributeA->isSmallerOrEqualThan(attributeB);
			break;
		default:
			//Houve erro na passagem do id do operador
			return false;
			break;		
	}	
	return valueLogic;	
}

void Premise::setInit(bool init){
	this->init = init;	
}

void Premise::setActive(bool active){
	this->active = active;
}

void Premise::setExclusive(bool exclusive){
	this->exclusive = exclusive;	
}

void Premise::setLogicValue(bool logicValue){
	this->logicValue = logicValue;
}

void Premise::setName(char* name){
	this->name = name;
}

bool Premise::isInit(){
	return this->init;	
}

bool Premise::isActive(){
	return this->active;
}

bool Premise::isExclusive(){
	return this->exclusive;
}

bool Premise::getLogicValue(){
	return this->logicValue;
}

char* Premise::getName(){
	return this->name;
}

void Premise::addSubCondition(SubCondition* anySubCondition){
	addCondition(anySubCondition);
}

void Premise::addCondition(Condition *anyCondition){
	conditionList->push_back(anyCondition);
	numOfConditions++;
}

int Premise::getNumOfConditions() const
{
  return numOfConditions; 
}

int Premise::incNumOfConditions()
{
  return numOfConditions++; 
}

//Robson R. L. 17-11-2010
//Este m�todo desconecta esta premissa de uma determinada condi��o 
//que est� sendo destru�da juntamente com a sua regra
void Premise::removeCondition(Condition * cond){
    conditionList->removeElement(cond);
    numOfConditions--;
}

#include "altera_avalon_performance_counter.h"
#include "system.h"
void Premise::notifyConditions(){
	
	bool newLogicValue = logicCalculus();

	if(getLogicValue() != newLogicValue){
		setLogicValue(newLogicValue);

		// Witt (15-4-2011) Modificada a forma de navega��o na lista		   
		//while (conditionList->hasNext()){
		MyList<Condition>::Iterator It;
		for( It.setCurrent( conditionList->getFirst() ); It.getInfo() != NULL; It.Next() ){

			//tmpCondition = conditionList->Next();
			tmpCondition = It.getInfo();

			if(getLogicValue())
				tmpCondition->incrementElementsTrue();						
			else
				tmpCondition->decrementElementsTrue();				 
		}

		// Navega na lista de condi~tions chamando o metodo logicCalculus
		for( It.setCurrent( conditionList->getFirst() ); It.getInfo() != NULL; It.Next() )
			(It.getInfo())->logicCalculus();
	}
//	PERF_STOP_MEASURING (PERFORMANCE_COUNTER_0_BASE);
}

void Premise::renotifyConditions(){
 	bool newLogicValue = logicCalculus();

	// Witt (15-4-2011) Modificada a forma de navega��o na lista	   
	//while (conditionList->hasNext()){
	MyList<Condition>::Iterator It;   
	for( It.setCurrent( conditionList->getFirst() ); It.getInfo() != NULL; It.Next() )
		(It.getInfo())->renotifyLogicCalculus();	
}


void Premise::renotifyConditionsAboutPremiseExclusive(){
	bool newLogicValue = logicCalculus();
	bool aux = false;

	// Witt (15-4-2011) Modificada a forma de navega��o na lista	   
	//while (conditionList->hasNext()){
	MyList<Condition>::Iterator It;   
	for( It.setCurrent( conditionList->getFirst() ); It.getInfo() != NULL; It.Next() ){
		//aux = (conditionList->Next())->renotifyLogicCalculus2();	
		aux = (It.getInfo())->renotifyLogicCalculus2();
		if(aux == true){
			aux = false;	
			break;
		}
	}    
}


void Premise::notifyConditionsAboutPremiseExclusive(){
	bool newLogicValue = logicCalculus();

	if(getLogicValue() != newLogicValue){
		setLogicValue(newLogicValue);
		Condition* tmpCondition = 0; 

		// Witt (15-4-2011) Modificada a forma de navega��o na lista	   
		//while (conditionList->hasNext()){
		MyList<Condition>::Iterator It;   
		for( It.setCurrent( conditionList->getFirst() ); It.getInfo() != NULL; It.Next() ){

			//tmpCondition = conditionList->Next();		
			tmpCondition = It.getInfo();

			if(getLogicValue()){
				tmpCondition->incrementElementsTrue();				 	
				tmpCondition->logicCalculus();
				if(tmpCondition->getLogicValue()){
					this->conditionExclusive = tmpCondition;
					break;
				}
			}				
			else{
				conditionExclusive->decrementElementsTrue();				 
				conditionExclusive->logicCalculus();
				break;
			}
		}
	}
}
