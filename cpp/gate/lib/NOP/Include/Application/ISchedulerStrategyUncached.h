#ifndef _ISCHEDULERSTRATEGYUNCACHED_H_
#define _ISCHEDULERSTRATEGYUNCACHED_H_

#include "ISchedulerStrategy.h"

class SchedulerStrategyUncached : public SchedulerStrategy{

private:
	Rule* rule;
public:
	SchedulerStrategyUncached(void);
	~SchedulerStrategyUncached(void);
	
	void execute();
	void addRuleToExecutionList(Rule* rule);
	void removeRuleToExecutionList(Rule* rule);
};

#endif
