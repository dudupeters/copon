#ifndef _IAPPLICATION_H_
#define _IAPPLICATION_H_

#include "IScheduler.h"
#include "IAttribute.h"
#include "IPremise.h"
#include "IInstigation.h"
#include "IBoolean.h"

/**
 * This abstract class is the extension point of the framework. 
 * It offers methods to the definition of the Rules and FBEs of the application to be developed. 
 * @author  Roni Fabio Banaszewski
 * @version  1.0
 * @see ConcreteApplication
 */

class Application{

public:
   
	/** 
	* A constructor 
	* @param scheduler The address of the scheduler to be used to schedule the execution of the approved Rules.
	*/
	Application(Scheduler* scheduler);
   
	/** 
    * A constructor 
    * @param schedulerStrategy Identifier of the strategy to be used by the scheduler
    */
	Application(int schedulerStrategy);
	
	/** A destructor */
	~Application(void);
	
	/* It invokes the methods related to instanciation of Rules and FBE and programming code in correct order to run applications */
	void startApplication();
	/* Place for definition of the rules*/
	virtual void initRules() = 0;
	/* Place for definition of the FBEs*/
	virtual void initFactBase() = 0;
	/* Place for definition of the Premises to be Shared by Conditions*/
	virtual void initSharedPremises() = 0;
	/* Place for definition of the Action related to the initialization's Rule */
	virtual void configureStartApplicationAction() = 0;
	/* Place for definition of the code of the application*/
	virtual void codeApplication() = 0;
	
	/* Get the object instance of the subclass of Scheduler
	   @return The scheduler used in the application
	*/
	Scheduler* getScheduler();
	
private:
	void approveStartRule();
	void initStartApplicationComponents();

protected:
	Scheduler* scheduler;

	//Definição dos componentes da Rule para inicialização da Aplicação
	/* Boolean Attribute of the Rule of initialization */
	Boolean* atStartApplication;
    /* Premise of the Rule of initialization */
	Premise* prStartApplication;
	/* Rule of initialization 
 	*  This Rule is used to start the application in order to initialize some Attributes
	*/
	Rule* rlStartApplication;
	/* Condition of the Rule of initialization */
	Condition* cdStartApplication;
	/* Action of the Rule of initialization */
	Action* acStartApplication;
    /** Add Instigations to the Action of the Rule of initialization
	 * @param instigation Instigation object
	 */
	void addInstigationToStartAction(Instigation* instigation);
};

#endif
