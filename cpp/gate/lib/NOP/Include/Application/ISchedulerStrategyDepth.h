#ifndef _ISCHEDULERSTRATEGYDEPTH_H_
#define _ISCHEDULERSTRATEGYDEPTH_H_

#include "ISchedulerStrategy.h"
#include <stack>
using namespace std;
using std::stack;

/**
 * This class represents the Conflict Resolution Strategy based in FILO (First-In Last-Out).  
 * The approved Rules are inserted in the top of the stack and removed at the top to be fired
 * @author  Roni Fabio Banaszewski
 * @version  1.0
 * @see SchedulerStrategy, SchedulerStrategyBreadth, SchedulerStrategyPriority
 */

class SchedulerStrategyDepth : public SchedulerStrategy{

private:
	Rule* rule;
	stack<Rule*> listRules;

public:
	SchedulerStrategyDepth(void);
	~SchedulerStrategyDepth(void);
	
	void execute();
	void addRuleToExecutionList(Rule* rule);
	void removeRuleToExecutionList(Rule* rule);	
};

#endif
