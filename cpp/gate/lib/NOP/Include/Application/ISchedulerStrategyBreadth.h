#ifndef _ISCHEDULERSTRATEGYBREADTH_H_
#define _ISCHEDULERSTRATEGYBREADTH_H_

#include "ISchedulerStrategy.h"
#include <queue>
using namespace std;
using std::queue;

/**
 * This class represents the Conflict Resolution Strategy based in FIFO (First-In First-Out).  
 * The approved Rules are inserted at final of the queue and removed at the front to be fired
 * @author  Roni Fabio Banaszewski
 * @version  1.0
 * @see SchedulerStrategy, SchedulerStrategyDepth, SchedulerStrategyPriority
 */

class SchedulerStrategyBreadth : public SchedulerStrategy{

private:
	queue<Rule*> listRules;
public:
	SchedulerStrategyBreadth(void);
	~SchedulerStrategyBreadth(void);
	
	void execute();
    void addRuleToExecutionList(Rule* rule);
	void removeRuleToExecutionList(Rule* rule);	
};

#endif
