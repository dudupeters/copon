#ifndef _ISCHEDULERSTRATEGY_H_
#define _ISCHEDULERSTRATEGY_H_

class Rule;

/**
 * Abstract class used to represent the conflict resolution strategies  
 * @author  Roni Fabio Banaszewski
 * @version  1.0
 * @see SchedulerStrategyDepth, SchedulerStrategyBreadth, SchedulerStrategyPriority 
 */

class SchedulerStrategy{

public:
	SchedulerStrategy(void);
	~SchedulerStrategy(void);
	
	virtual void execute() = 0;
	/** Add approved Rules to a defined data structure related to a certain strategy*/
	virtual void addRuleToExecutionList(Rule* rule) = 0;
	/** Remove Rules to a defined data structure related to a certain strategy*/
	virtual void removeRuleToExecutionList(Rule* rule) = 0;

	/** Identify the breadht Conflict Resolution Strategy*/
	const static int BREADTH = 0;
	/** Identify the priority Conflict Resolution Strategy*/
	const static int PRIORITY = 1;
	/** Identify the depth Conflict Resolution Strategy*/
	const static int DEPTH = 2;
	/** Identify the uncached Conflict Resolution Strategy*/
	const static int UNCACHED = 3;	
	/** Identify that no one Conflict Resolution Strategy based on data structures is used
	* In this strategy, the Rule is fired imediately it is approved.
	*/
	const static int NO_ONE = 4;	

protected:
	bool isInferenceExecuting;	
};

#endif
