#ifndef _IMETHODPOINTER_H_
#define _IMETHODPOINTER_H_

#include "IMethod.h"


class Fact;

template<class T>
class MethodPointer : public Method{
	protected:
      typedef void (T:: *PointerToMethod) ();
	  typedef void (T:: *PointerToMethodParameters) (MyList< Attribute > *parameters);	  
	  typedef void (T:: *PointerToMethodInteger) (int);
	  
      T* fact;
      PointerToMethod pointerToMethod; 
	  PointerToMethodParameters pointerToMethodParameters;
	  PointerToMethodInteger pointerToMethodInteger;
	  
   public:
       MethodPointer(T* fact, PointerToMethod pointerToMethod):
	   Method(0)
       {
         MethodPointer::fact = fact;
         MethodPointer::pointerToMethod = pointerToMethod;
       }

	   MethodPointer(T* fact, PointerToMethodParameters pointerToMethodParameters):
	   Method()
       {
         MethodPointer::fact = fact;
         MethodPointer::pointerToMethodParameters = pointerToMethodParameters;
       }

	   MethodPointer(T* fact, PointerToMethodInteger pointerToMethodInteger):
	   Method()
       {
         MethodPointer::fact = fact;
         MethodPointer::pointerToMethodInteger = pointerToMethodInteger;
       }

	  MethodPointer():Method() { }
      ~MethodPointer() { }
      
	  virtual void execute();
	  void execute(MyList< Attribute > *parameters);
	  void execute(int parameter);
};

template<class T>
void MethodPointer<T>::execute(){
   if (0 != pointerToMethod)   {
	   //executing = true;  	  
      (fact->*pointerToMethod) (); 	 	  
   }
}

template<class T>
void MethodPointer<T>::execute(MyList< Attribute > *parameters){
	if (0 != pointerToMethodParameters)   {
	   //executing = true;  	  
      (fact->*pointerToMethodParameters) (parameters); 	 	  
    }
}

template<class T>
void MethodPointer<T>::execute(int parameter){
   	if (0 != pointerToMethodInteger)   {
	   //executing = true;  	  
      (fact->*pointerToMethodInteger) (parameter); 	 	  
   }
}

#endif
