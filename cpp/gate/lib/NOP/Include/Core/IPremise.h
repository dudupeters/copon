#ifndef _IPREMISE_H_
#define _IPREMISE_H_

#include "ICondition.h"
#include "ISubCondition.h"
#include "MyList.h"

using namespace std;
using namespace std;

class Attribute;
class Boolean;
class Integer;
class Double;
class SubCondition;
class Condition;

class Premise{
  
 protected:				
		bool logicValue, init, active;
		bool exclusive;
		int operattor;
		char* name;
		Condition* conditionExclusive;
		Condition* tmpCondition; 	
		MyList<Condition> *conditionList;
		int numOfConditions;

 public:
		Attribute* attributeA;
		Attribute* attributeB;

		const static int EQUAL = 0;
		const static int DIFFERENT = 1;
		const static int GREATERTHAN = 2;
		const static int SMALLERTHAN = 3;
		const static int GREATEROREQUAL = 4;
		const static int SMALLEROREQUAL = 5;

		const static int FIRST_ONLY = 6;
		const static int SECOND_ONLY = 7;
		const static int NO_ONE = 8;
		const static int BOTH = 9;

 public:
	    Premise(char *name, Attribute* attributeA, Attribute* attributeB, int operattor, bool exclusive = false);
		Premise(Attribute* attributeA, Attribute* attributeB, int operattor, bool exclusive = false); 	
		Premise(Attribute* attributeA, bool atBoolean, int operattor, bool exclusive = false);
		Premise(Attribute* attributeA, int atInteger, int operattor, bool exclusive = false);
		Premise(Attribute* attributeA, double atDouble, int operattor, bool exclusive = false);
		Premise(Attribute* attributeA, char atChar, int operattor, bool exclusive = false);
		Premise(Attribute* attributeA, char* atString, int operattor, bool exclusive = false);

		 //TODO Glauber: revisar para certificar-se de que os links para esta premissa est�o
         //sendo removidos nos atributos pertinentes
		~Premise();

		void setInit(bool init);
		void setActive(bool active);
		void setLogicValue(bool logicValue);
		void setExclusive(bool exclusive);		
		void setName(char* name);

		bool isInit();
		bool isActive();
		bool isExclusive();
		bool getLogicValue();
		char* getName();
		void notifyConditions();
		void renotifyConditions();
		void notifyConditionsAboutPremiseExclusive();
		void renotifyConditionsAboutPremiseExclusive();
		bool logicCalculus();
		bool changeValue(bool value);
		void addSubCondition(SubCondition* anySubCondition);
		void addCondition(Condition* anyCondition);

		int getNumOfConditions() const;
        int incNumOfConditions();
        void removeCondition(Condition * cond);
};

#endif
