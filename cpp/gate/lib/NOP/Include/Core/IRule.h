#ifndef _IRULE_H_
#define _IRULE_H_

#include "IScheduler.h"
#include "ICondition.h"
#include "IAction.h"
#include "IBoolean.h"
#include <iostream>

class Scheduler;
class Action;
class Condition;
class FBE;

class Rule{

public:
	const static int LOW = 0;
	const static int MEDIUM = 1;
	const static int HIGH = 2;

private:
	static int amountOfRules;    
	bool isDerivedd; //verifica se a regra foi derivada

protected:
	char* name;
	bool executed, executing, approvedd;
	int priority;
	int id;	
	bool start;
	
	Condition* condition;
	Action* action;
	Scheduler* scheduler;

public:
	// Modificado por Fernando Augusto de Witt - 05/02/2010
	// Adicionei o IsDereved na construtora, isso foi necessario, pois acredito que a parte
	// da regra derivada ainda n�o havia sido inicializada. Isso fazia com que o metodo mais
	// especializado n�o fosse chamado.
	Rule(char* name, Scheduler* scheduler, Condition* condition, Action* action, bool IsDerived = false);
	Rule(char* name, Scheduler* scheduler, Condition* condition);
	Rule(char* name, Scheduler* scheduler, Condition* condition, Action* action, int priority);
	~Rule(void);

	Condition* getCondition();
	bool approved();
	bool reproved();
	virtual void execute();
	bool isExecuted();
	bool isExecuting();
	bool isApproved(); 
	bool isStart();
	bool isDerived();
	int getPriority();
	int getId();
	void setExecuting(bool executing);
	void setApproved(bool approvedd);
	void setPriority(int priority);
	void setStart(bool init);
	void end();
	char* getname(){ return name; }
	
	virtual void executeRule();
	static int getAmountOfRules();

};

#endif
