#ifndef _ICHAR_H_
#define _ICHAR_H_

#include "IAttribute.h"

/**
 * This class represents the primitive char data type with capacities of reactiveness, decoupling and collaborations by notifications.
 * @author  Roni Fabio Banaszewski
 * @version  1.0
 */

class Char : public Attribute {

public:
	Char();
	Char(char value);
	Char(FBE* fact, char value);
	~Char(void);

	double getValue();
	void setValue(char value);
	void setValue(Attribute* value);
	void setValue(char value, int flag);
	void setValue(Attribute* value, int flag);
	bool isEqual(Attribute* AttributeB);
	bool isGreaterThan(Attribute* attributeB);
	bool isSmallerThan(Attribute* attributeB);
	bool isGreaterOrEqualThan(Attribute* attributeB);
	bool isSmallerOrEqualThan(Attribute* attributeB);
	bool isDifferent(Attribute* attributeB);
	
private:
	char myChar;
};

#endif