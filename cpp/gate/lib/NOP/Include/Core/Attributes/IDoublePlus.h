#ifndef _IDOUBLE_PLUS_H_
#define _IDOUBLE_PLUS_H_

#include "IDouble.h"

class DoublePlus : public Double {

public:

	DoublePlus();
	DoublePlus(double value);
	DoublePlus(FBE* fact, double value);
	DoublePlus(Double& attr);
	~DoublePlus(void);

	void add(Double* value, int flag = 0);
	void add(double value, int flag = 0);

	void subtract(Double* value, int flag = 0);
	void subtract(double value, int flag = 0);
	
	void multiply(Double* value, int flag = 0);
	void multiply(double value, int flag = 0);
	
	void divide(Double* value, int flag = 0);
	void divide(double value, int flag = 0);

};

#endif
