#ifndef _ISTRING_H_
#define _ISTRING_H_

#include "IAttribute.h"

#include <cstring>
using std::strcmp;
using std::strcpy;
using std::strcat;

/**
 * This class represents the String data type with capacities of reactiveness, decoupling and collaborations by notifications.
 * @author  Roni Fabio Banaszewski
 * @version  1.0
 */

class String : public Attribute{

public:
    String();
	String(char* value);
	String(FBE* fact, char* value);
	~String(void);

	char* getValue();
	void setValue(char* value);
	void setValue(Attribute* value);
	void setValue(char* value, int flag);
	void setValue(Attribute* value, int flag);
	bool isEqual(Attribute* AttributeB);
	bool isGreaterThan(Attribute* attributeB);
	bool isSmallerThan(Attribute* attributeB);
	bool isGreaterOrEqualThan(Attribute* attributeB);
	bool isSmallerOrEqualThan(Attribute* attributeB);
	bool isDifferent(Attribute* attributeB);
	bool isValueChange(Attribute* value);

private:
	char* myString;	
};

#endif
