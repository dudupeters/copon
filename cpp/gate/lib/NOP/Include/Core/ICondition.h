#ifndef _ICONDITION_H_
#define _ICONDITION_H_

#include "ILogicalOperator.h"
#include "IRule.h"
#include "IConjunction.h"
#include "IDisjunction.h"
#include "MyList.h"
#include "ISingle.h"
//using namespace std;
//using namespace std;

class SubCondition;
class Rule;
class Premise;

class Condition {
 
public:
	    const static int CONJUNCTION = 0;
		const static int DISJUNCTION = 1;
		const static int SINGLE = 2;

private:		
		MyList<Premise> *premiseList;	   

protected:
		bool logicValue;
		int amountElements, amountElementsTrue;
		
		LogicalOperator* logicalOperator;
		Rule* rule;

public:		
		Condition(LogicalOperator* logicalOperator);
		Condition(int operattor);
		//TODO Glauber: revisar destrutora para remover premissas que s� 
        //est�o ligadas a esta condi��o               
		~Condition(void);     
		
		void addSubCondition(SubCondition* anySet);
		bool getLogicValue();
		void setLogicValue(bool logicValue);
		void setRule(Rule* rule);
		void incrementElementsTrue();
		void decrementElementsTrue();
		void incrementElements();
		void decrementElements();
		int getAmountElements();

		virtual void addPremise(Premise* anyPremise);
		virtual void logicCalculus();
		virtual void renotifyLogicCalculus();
		virtual bool renotifyLogicCalculus2();	
 
};

#endif
