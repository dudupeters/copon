#ifndef _ISCHEDULER_H_
#define _ISCHEDULER_H_

#include "IRule.h"
#include "ISchedulerStrategyBreadth.h"
#include "ISchedulerStrategyPriority.h"
#include "ISchedulerStrategyDepth.h"
#include "ISchedulerStrategyUncached.h"

class SequentialScheduler;
class Rule;
class SchedulerStrategy;

class Scheduler{

protected:	 
	SchedulerStrategy* strategy;
	int type;
	bool ruleInExecution;	

public:
	Scheduler();
	Scheduler(int strategy);
	~Scheduler(void);
	
	void approveRule(Rule* rule);
	void removeApprovedRule(Rule* rule);
	bool hasRuleInExecution();
	void execute();
	int getType();

	SchedulerStrategy* getStrategy();
};

#endif
