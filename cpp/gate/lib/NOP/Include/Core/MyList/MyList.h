#include "Element.h"
#include <stddef.h>

#pragma once

template <class TIPO>
class MyList
{
private:
	Element<TIPO>* pfirst;
	Element<TIPO>* plast;	
	Element<TIPO>* current;	
	int size;

public:
	MyList(void);
	Element<TIPO>* getFirst();
	Element<TIPO>* getLast();
	bool addElment (Element<TIPO> *el);
	TIPO* getElement(int i);
	bool removeElement(TIPO *el);
	bool hasNext();
	TIPO* Next();
	int getSize();
	bool isEmpty();
	void deleteAll();
	void push_back(TIPO *info);
	virtual ~MyList(void);	

	class Iterator{
	private:
		Element<TIPO>* Current;
	public:
		Iterator(void){
			Current = 0;
		}
		void setCurrent( Element<TIPO>* pCurrent ){
			Current = pCurrent;
		}
		void Next(void){
			if( Current != 0 )
				Current = Current->getprox();
		}
		void Back(void){
			if( Current != 0 )
				Current = Current->getant();
		}
		TIPO* getInfo(void){
			if( Current != 0 )
				return Current->getinfo();
			return 0;
		}
	};
};

template <class TIPO>
Element<TIPO>* MyList<TIPO>::getFirst(){
	return pfirst;
}

template <class TIPO>
Element<TIPO>* MyList<TIPO>::getLast(){
	return plast;
}

template <class TIPO>
MyList<TIPO>::MyList(){
	pfirst = 0;
	plast = 0;	
	current = 0;
	size = 0;
}

template <class TIPO>
void MyList<TIPO>::push_back(TIPO *info){
	Element<TIPO> *el = new Element<TIPO>();	
	el->setinfo(info);
	addElment(el);
}

template <class TIPO>
bool MyList<TIPO>::addElment(Element<TIPO> *el){
	if (pfirst != 0){
		el->setant(plast);
		el->setprox(0);
		plast->setprox(el);
		plast = el;
	}
	else
	{
		pfirst = el;
		pfirst->setant(0);
		pfirst->setprox(0);		
		plast = el;
		current = pfirst;		
	}
	size++;
	return true;
}


template <class TIPO>
TIPO* MyList<TIPO>::getElement(int i)
{
	Element<TIPO> *aux = pfirst;
	int cont = 0;
	if (i != 0)
	{
	  while (true) {
			aux = aux->getprox();
			cont ++;
			if (cont == i)  
				break;
	  }
	}	
	return aux->getinfo();
}

template <class TIPO>
bool MyList<TIPO>::hasNext()
{	
	bool b = false;
	if (current == 0)			
		current = pfirst;
	else			
		b = true;
	return b;
}


template <class TIPO>
TIPO* MyList<TIPO>::Next()
{
	TIPO * tipo;
	if (current == 0)			
		return 0;
	else			
		tipo = current->getinfo();
	
	current = current->getprox();
	return tipo;
}


template <class TIPO>
int MyList<TIPO>::getSize()
{
	return size;
}

template <class TIPO>
bool MyList<TIPO>::isEmpty()
{
	if (pfirst == NULL)
		return true;
	else
		return false;

}

template <class TIPO>
void MyList<TIPO>::deleteAll()
{
	Element<TIPO> *aux;
	aux = pfirst;
	while(aux){
		pfirst = pfirst->getprox();
		delete(aux);
		aux = pfirst;
	}
	size = 0;
	current = NULL;
}


template <class TIPO>
bool MyList<TIPO>::removeElement(TIPO *el){
	
        Element<TIPO> *aux = pfirst;
        Element<TIPO> *previous = 0;
        Element<TIPO> *next;
        while (aux != 0)
        {
          next = aux->getprox();
          if (aux->getinfo() == el)
          {
             if (aux == pfirst)
             {
                pfirst = next; 
             }
             else
             {
                previous->setprox(next); 
             }
             if (aux == plast)
             {
                plast = previous; 
             }
             else
             {
                next->setant(previous);
             }
             return true;
          }
          else
          {
             previous = aux;
             aux = aux->getprox();
          }
        }
  
	return false;
}

template <class TIPO>
MyList<TIPO>::~MyList(){
	Element<TIPO> *aux;
	aux = pfirst;
	while(aux){
		pfirst = pfirst->getprox();
		delete(aux);
		aux = pfirst;
	}	
	current = 0;
}
