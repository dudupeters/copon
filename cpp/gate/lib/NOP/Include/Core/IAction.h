#ifndef _IACTION_H_
#define _IACTION_H_

#include "IRule.h"
#include "IInstigation.h"

#include "MyList.h"
using namespace std;


class Action{

private:
	bool executed;
	Rule* rule;
	MyList<Instigation> *instigationList; 	
	
public:
	Action(void);
	~Action(void);

	void setRule(Rule* rule);
	void setExecuted(bool executed);
	void addInstigation(Instigation* instigation);
	bool isExecuted();
	void execute();
};

#endif
