#ifndef _ISUBCONDITION_H_
#define _ISUBCONDITION_H_

#include "ICondition.h"
#include "IPremise.h"

#include <list>
using namespace std;
using std::list;

class Condition;
class Premise;


/**
 * This class allow to compose complex Conditions, i.e. Conditions that demand the use of more than one logical operator(Conjunction, Disjunction and Single).<BR>
 * A SubCondition represents a grouping of Premises of a Condition that are related by a commum logical operator.<BR>
 * The following Rule exemplify the SubCondition use: <BR>   
 * ...<BR>
 * 1 Rule* rlStop = new Rule(Condition::CONJUNCTION);<BR>
 * 2 rlStop->addSubCondition(Condition::SINGLE);<BR>
 * 3 rlStop->addPremiseToSubCondition(semaphore->atSignal, "RED", Premise::EQUAL);<BR>
 * 4 rlStop->addSubCondition(Condition::DISJUNCTION);<BR>
 * 5 rlStop->addPremiseToSubCondition(car->atCurve, "LEFT", Premise::EQUAL);<BR>
 * 6 rlStop->addPremiseToSubCondition(semaphore->atCurve, "RIGHT", Premise::EQUAL);<BR>
 * 7 rlStop->addPremiseToSubCondition(semaphore->atCurve, "NO", Premise::EQUAL);<BR>
 * 8 rlStop->addInstigation(car->mtStop);<BR>
 * 9 rlStop->end();<BR>
 * ...<BR>
 * As illustrated in this Rule, the SubConditions use is completely intuitive. Firstly, it is need to define the logical operator of the SubConditions (lines 2 and 4) and after, 
 * it is need to connect the respective Premises to these SubConditions. Thus, the Premises are able to notify the Condition indirectly by means of their SubConditions.<BR>
 * In this sense, the SubConditions will act as Premises, notifying  the respective Conditions when are changed. <BR>
 * To a Condition, it is indiferent receive notification from SubConditions or Premises, once their logical states are updated in the same way.
 * @author  Roni Fabio Banaszewski
 * @version  1.0
 */

class SubCondition : public Condition{
	
private:
	bool exclusive;
	list<Condition*> conditionList;
	list<Condition*>::iterator it;
	

public:
	SubCondition(LogicalOperator* logicalOperator, bool exclusive);	
	SubCondition(int operattor, bool exclusive);
	~SubCondition(void);

	void logicCalculus();
	bool calculateLogicalValue();
	void addPremise(Premise* premise);
	void addCondition(Condition* condition);
	void notifyConditions();
	bool isExclusive();
	void setExclusive(bool exclusive);
};

#endif
