#ifndef _ICONJUNCTION_H_
#define _ICONJUNCTION_H_

#include "ILogicalOperator.h"

/**
 * This class represents a logical operator of conjunction in a Condition.
 * @author Roni Fabio Banaszewski
 * @version 1.0
 * @see Condition
 */

class Conjunction : public LogicalOperator {
public:
	Conjunction();
	~Conjunction(void);
    /**
	 * Verify if the respective Condition is true or approved.
	 */
	bool isTrue(int amountElements, int amountElementsTrue); //m�todo herdado de LogicalOperator
};

#endif
