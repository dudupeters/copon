#ifndef _ILOGICALOPERATOR_H_
#define _ILOGICALOPERATOR_H_

class Conjunction;
class Disjunction;

/**
 * This class represents a logical operator of disjunction in a Condition.
 * @author Roni Fabio Banaszewski
 * @version 1.0
 * @see Condition
 */
class LogicalOperator{
public:
	LogicalOperator();
	~LogicalOperator(void);

	virtual bool isTrue(int amountElements, int amountElementsTrue) = 0;
};

#endif
