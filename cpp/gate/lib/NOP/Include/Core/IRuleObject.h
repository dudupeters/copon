#ifndef _IRULEOBJECT_H_
#define _IRULEOBJECT_H_

#include "ICondition.h"
#include "IPremise.h"
#include <map>
#include <vector>

class RuleObject : public Rule, public FBE{

public:
	
	MyList <Attribute> attributes;
	SubCondition* subCondition;
	MyList<RuleObject> *dependentRulesList;

	FBE* masterRuleExecuted;
	Boolean* atMasterRuleExecuted;
	
public:
	RuleObject(char* name, Scheduler* scheduler,int logicalOperator);
	~RuleObject(void);	

	void addPremise(char* name, Attribute* attributeA, Attribute* attributeB, int operattor, bool exclusive, int attributesToSave = Premise::NO_ONE);
	void addPremise(Attribute* attributeA, Attribute* attributeB, int operattor, bool exclusive, int attributesToSave  = Premise::NO_ONE);
	void addPremise(Attribute* attributeA, bool atBoolean, int operattor, bool exclusive, int attributesToSave = Premise::NO_ONE);
	void addPremise(Attribute* attributeA, int atInteger, int operattor, bool exclusive, int attributesToSave = Premise::NO_ONE);
	void addPremise(Attribute* attributeA, double atDouble, int operattor, bool exclusive, int attributesToSave = Premise::NO_ONE);
	void addPremise(Attribute* attributeA, char atDouble, int operattor, bool exclusive, int attributesToSave = Premise::NO_ONE);
	void addPremise(Attribute* attributeA, char* atDouble, int operattor, bool exclusive, int attributesToSave = Premise::NO_ONE);
	void addPremise(Premise* premise);
	void addInstigation(Instigation* instigation);
	void addInstigation(Boolean *attribute, bool bValue);
	void addInstigation(Integer *attribute, int iValue);
	void addInstigation(Double *attribute, double dValue);
	void addInstigation(Char *attribute, char cValue);
	void addInstigation(String *attribute, char* sValue);
	void addMethod(Method* method);
	void addMethodWithParameters(Method* method);
	
	void addSubCondition(int logicalOperator, bool exclusive);
	//void addSubCondition(LogicalOperator logicalOperator, bool exclusive);
	void addPremiseToSubCondition(Attribute* attributeA, Attribute* attributeB, int operattor, bool exclusive);
	void addPremiseToSubCondition(Attribute* attributeA, bool atBoolean, int operattor, bool exclusive);
	void addPremiseToSubCondition(Attribute* attributeA, int atInteger, int operattor, bool exclusive);
	void addPremiseToSubCondition(Attribute* attributeA, double atDouble, int operattor, bool exclusive);
	void addPremiseToSubCondition(Premise* premise);

	void execute();
	void addMasterRule( RuleObject* masterRule );
	void notifyDependentRules();

};

#endif
