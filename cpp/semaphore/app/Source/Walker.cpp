#include "../Include/IWalker.h"

Walker::Walker()
{
	
	atStatus = new Boolean( this, false );
	mtMove = new MethodPointer< Walker >( this, &Walker::move );
	mtStop = new MethodPointer< Walker >( this, &Walker::stop );

}

void Walker::move()
{
	
	cout << "# The walker is crossing the road." << endl;
	atStatus->setValue( Boolean::TRUE_NOP );

}

void Walker::stop()
{
	
	cout << "# The walker stopped." << endl;
	atStatus->setValue( Boolean::FALSE_NOP );

}
