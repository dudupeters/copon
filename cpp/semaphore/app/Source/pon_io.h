/*
 * pon_io.h
 *
 *  Created on: Dec 8, 2011
 *      Author: peters
 */

#ifndef PON_IO_H_
#define PON_IO_H_

//#include "altera_avalon_pio_regs.h"

#define read_switches()						IORD_ALTERA_AVALON_PIO_DATA(SWITCH_PIO_BASE)
#define read_buttons()						~IORD_ALTERA_AVALON_PIO_DATA(BUTTON_PIO_BASE)

#define write_red_leds(data)				IOWR_ALTERA_AVALON_PIO_DATA(LED_RED_BASE, data)
#define write_green_leds(data)				IOWR_ALTERA_AVALON_PIO_DATA( LED_GREEN_BASE, data );
#define write_seven_segments(data)			IOWR_ALTERA_AVALON_PIO_DATA(SEG7_DISPLAY_BASE, data);

#endif /* PON_IO_H_ */
