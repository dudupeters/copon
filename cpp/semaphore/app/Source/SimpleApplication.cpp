#include <stdio.h>
#include "system.h"
#include <stddef.h>
#include <iostream>
#include <unistd.h>
#include "pon_io.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_performance_counter.h"

#include "../Include/ISimpleApplication.h"

SimpleApplication::SimpleApplication(int schedulerStrategy) :
	Application(schedulerStrategy) {

	startApplication();

}

void SimpleApplication::initRules() {

	// Premissas.
	Premise* premiseSemaphoreOpened = new Premise(semaphore->atStatus, GREEN,
			Premise::EQUAL);
	Premise* premiseSemaphoreClosed = new Premise(semaphore->atStatus, RED,
			Premise::EQUAL);

	// Instiga��es.
	Instigation* instigationCarMove = new Instigation(car->mtMove);
	Instigation* instigationCarStop = new Instigation(car->mtStop);
	Instigation* instigationWalkerStop = new Instigation(walker->mtStop);
	Instigation* instigationWalkerMove = new Instigation(walker->mtMove);
	Instigation* instigationCloseSemaphore =
			new Instigation(semaphore->mtClose);
	Instigation* instigationOpenSemaphore = new Instigation(semaphore->mtOpen);

	/*************************************************************************************************************************************/

	// Sem�foro aberto - sem�foro fecha.
	RuleObject* ruleSemaphoreClose = new RuleObject(
			"Semaphore opened - semaphore close", scheduler,
			Condition::CONJUNCTION);
	ruleSemaphoreClose->addPremise(premiseSemaphoreOpened);
	ruleSemaphoreClose->addInstigation(instigationCloseSemaphore);

	// Sem�foro fechado - carro para.
	RuleObject* ruleCarStop = new RuleObject("Semaphore closed - car stop",
			scheduler, Condition::SINGLE);
	ruleCarStop->addPremise(premiseSemaphoreClosed);
	ruleCarStop->addInstigation(instigationCarStop);

	// Sem�foro fechado - pedestre vai.
	RuleObject* ruleWalkerMove =
			new RuleObject("Semaphore closed - walker move", scheduler,
					Condition::CONJUNCTION);
	ruleWalkerMove->addPremise(premiseSemaphoreClosed);
	ruleWalkerMove->addMasterRule(ruleCarStop);
	ruleWalkerMove->addInstigation(instigationWalkerMove);

	/*************************************************************************************************************************************/

	// Sem�foro fechado - sem�foro abre.
	RuleObject* ruleSemaphoreOpen = new RuleObject(
			"Semaphore closed - semaphore open", scheduler,
			Condition::CONJUNCTION);
	ruleSemaphoreOpen->addPremise(premiseSemaphoreClosed);
	ruleSemaphoreOpen->addInstigation(instigationOpenSemaphore);

	// Sem�foro aberto - pedestre para.
	RuleObject* ruleWalkerStop = new RuleObject(
			"Semaphore opened - walker stop", scheduler, Condition::SINGLE);
	ruleWalkerStop->addPremise(premiseSemaphoreOpened);
	ruleWalkerStop->addInstigation(instigationWalkerStop);

	// Sem�foro aberto - carro se move.
	RuleObject* ruleCarMove = new RuleObject("Semaphore opened - car move",
			scheduler, Condition::CONJUNCTION);
	ruleCarMove->addPremise(premiseSemaphoreOpened);
	ruleCarMove->addMasterRule(ruleWalkerStop);
	ruleCarMove->addInstigation(instigationCarMove);

	/*************************************************************************************************************************************/

	ruleSemaphoreClose->end();
	ruleCarStop->end();
	ruleWalkerMove->end();
	ruleSemaphoreOpen->end();
	ruleWalkerStop->end();
	ruleCarMove->end();

}

void SimpleApplication::initFactBase() {

	car = new Car();
	semaphore = new Semaphore();
	walker = new Walker();

}

void SimpleApplication::initSharedPremises() {

}

void SimpleApplication::configureStartApplicationAction() {

}

void SimpleApplication::codeApplication() {

}

int main() {

	write_red_leds(0x0000);
	write_green_leds(0x0000);

	usleep(500000);
	write_red_leds(0xFF);

	cout << "Iniciando... ";

	PERF_RESET (PERFORMANCE_COUNTER_0_BASE);
	PERF_START_MEASURING (PERFORMANCE_COUNTER_0_BASE);
	SimpleApplication simpleApplication(SchedulerStrategy::NO_ONE);

	PERF_STOP_MEASURING (PERFORMANCE_COUNTER_0_BASE);
	long int totalTime = perf_get_total_time(alt_get_performance_counter_base());

	cout << "Ciclos de clock: " << totalTime << endl;

	cout << "END";
	write_green_leds(0xFF);

	return 0;

}
