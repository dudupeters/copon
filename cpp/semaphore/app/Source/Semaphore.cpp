#include "../Include/ISemaphore.h"

Semaphore::Semaphore()
{

	atStatus = new Integer( this, GREEN );
	mtOpen = new MethodPointer< Semaphore >( this, &Semaphore::open );
	mtClose = new MethodPointer< Semaphore >( this, &Semaphore::close );

}


void Semaphore::open()
{
	
	cout << "X Semaphore opened." << endl;
	//getchar();
	atStatus->setValue( GREEN );

}

void Semaphore::close()
{
	
	cout << "X Semaphore closed." << endl;
	atStatus->setValue( RED );

}
