#include "../Include/ICar.h"

Car::Car()
{

	atStatus = new Boolean( this, false );
	mtMove = new MethodPointer< Car >( this, &Car::move );
	mtStop = new MethodPointer< Car >( this, &Car::stop );

}

void Car::move()
{

	cout << "# The car is moving now." << endl;
	atStatus->setValue( Boolean::TRUE_NOP );
	
}

void Car::stop()
{
	
	cout << "# The car has been stopped." << endl;
	atStatus->setValue( Boolean::FALSE_NOP );
	
}
