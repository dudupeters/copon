#ifndef _ISEMAPHORE_H_
#define _ISEMAPHORE_H_

#include "IFBE.h"
#include "IInteger.h"
#include "IMethodPointer.h"

#include <iostream>

using namespace std;

const int RED = 0;
const int GREEN = 1;

class Semaphore : public FBE
{

	public:

		Integer* atStatus;

	public:
	
		Semaphore();

		MethodPointer< Semaphore >* mtOpen;
		MethodPointer< Semaphore >* mtClose;

	private:

		void open();
		void close();

};

#endif
