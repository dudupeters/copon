#ifndef _IWALKER_H_
#define _IWALKER_H_

#include "IFBE.h"
#include "IBoolean.h"
#include "IMethodPointer.h"

#include <iostream>

using namespace std;

class Walker : public FBE
{

	public:

		Boolean* atStatus;

	public:
	
		Walker();

		MethodPointer< Walker >* mtMove;
		MethodPointer< Walker >* mtStop;


	private:
	
		void move();
		void stop();
	
};

#endif
