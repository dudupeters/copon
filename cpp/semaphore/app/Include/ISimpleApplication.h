#ifndef _ISIMPLEAPPLICATION_H_
#define _ISIMPLEAPPLICATION_H_

#include "IApplication.h"
#include "IRuleObject.h"

#include "../Include/ICar.h"
#include "../Include/ISemaphore.h"
#include "../Include/IWalker.h"

#include <iostream>

using namespace std;

class SimpleApplication : public Application
{

	public:
		
		SimpleApplication( int schedulerStrategy );
		
		void initRules();
		void initFactBase();
		void initSharedPremises();
		void configureStartApplicationAction();
		void codeApplication();

	private:
		
		Car* car;
		Semaphore* semaphore;
		Walker* walker;

};

#endif
