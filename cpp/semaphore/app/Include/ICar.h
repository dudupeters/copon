#ifndef _ICAR_H_
#define _ICAR_H_

#include "IFBE.h"
#include "IBoolean.h"
#include "IMethodPointer.h"

#include <iostream>

using namespace std;

class Car : public FBE
{

	public:

		Boolean* atStatus;

	public:
	
		Car();

		void move();
		void stop();
	
		MethodPointer< Car >* mtMove;
		MethodPointer< Car >* mtStop;

};

#endif
