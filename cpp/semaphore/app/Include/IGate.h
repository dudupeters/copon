#ifndef _IGATE_H_
#define _IGATE_H_

#include "IFBE.h"
#include "IInteger.h"
#include "IMethodPointer.h"

class Gate: public FBE{
public:
	Integer* GateStatus;

public:
	Gate();
	void OpenGate();
	void CloseGate();
	MethodPointer<Gate>* mtOpenGate;
	MethodPointer<Gate>* mtCloseGate;
};

#endif
