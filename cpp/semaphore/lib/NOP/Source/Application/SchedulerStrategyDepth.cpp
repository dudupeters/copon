#include "ISchedulerStrategyDepth.h"
#include "IRule.h"

SchedulerStrategyDepth::SchedulerStrategyDepth(void): SchedulerStrategy(){
	this->rule = 0;
}

SchedulerStrategyDepth::~SchedulerStrategyDepth(void){
}

/**
 * Control the execution of Rules in the list. Extract the Rule on top of the list, if exist, and execute it.
 */
void SchedulerStrategyDepth::execute(){
	Rule* ruleTmp = 0;
	isInferenceExecuting = true;
	while(!listRules.empty()){
		ruleTmp = listRules.top();
		listRules.pop();
		if(ruleTmp->isDerived()){	
			ruleTmp->setExecuting(true);
			//ruleTmp->reproved();					
			ruleTmp->setExecuting(false);
			ruleTmp->executeRule();					
	    }
	    else{
			ruleTmp->setExecuting(true);
			//ruleTmp->reproved();					
			ruleTmp->setExecuting(false);
		    ruleTmp->execute();		 
	    } 
	}	 
	isInferenceExecuting = false;
}

void SchedulerStrategyDepth::addRuleToExecutionList(Rule *rule){
	listRules.push(rule);
	if(!isInferenceExecuting){
		execute();
	}
}

void SchedulerStrategyDepth::removeRuleToExecutionList(Rule *rule){
}
