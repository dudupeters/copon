#include "IConcreteApplication.h"

ConcreteApplication::ConcreteApplication(Scheduler* scheduler):Application(scheduler){
	this->startApplication();
}

ConcreteApplication::~ConcreteApplication(void){
	this->startApplication();
}

void ConcreteApplication::initFactBase(){
}

void ConcreteApplication::initRules(){
}

void ConcreteApplication::initSharedPremises(){
}

void ConcreteApplication::configureStartApplicationAction(){
}

void ConcreteApplication::codeApplication(){
}
