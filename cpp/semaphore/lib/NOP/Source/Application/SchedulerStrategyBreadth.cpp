#include "ISchedulerStrategyBreadth.h"
#include "IRule.h"

SchedulerStrategyBreadth::SchedulerStrategyBreadth(void): SchedulerStrategy(){
}

SchedulerStrategyBreadth::~SchedulerStrategyBreadth(void){
}

/**
 * Control the execution list. Extract each Rule of the list and execute it.
 */
void SchedulerStrategyBreadth::execute(){
 	Rule* ruleTmp = 0;
	isInferenceExecuting = true;
	
	while(!listRules.empty()){		
		ruleTmp = listRules.front();
		if(ruleTmp->isDerived()){	
			ruleTmp->setExecuting(true);
			ruleTmp->executeRule();
			ruleTmp->setExecuting(false);
			ruleTmp->reproved();					
	    }
	    else{
			ruleTmp->setExecuting(true);
			ruleTmp->execute();
			ruleTmp->setExecuting(false);
			ruleTmp->reproved();				 
	    } 
	}
	isInferenceExecuting = false;
}

/**
 * Insert the Rule to the last of the execution list.
 * @param rule instance of Rule
 */
void SchedulerStrategyBreadth::addRuleToExecutionList(Rule *rule){
	listRules.push(rule);	
	if(!isInferenceExecuting){
		execute();
	}
}

void SchedulerStrategyBreadth::removeRuleToExecutionList(Rule *rule){
	listRules.pop();
}
