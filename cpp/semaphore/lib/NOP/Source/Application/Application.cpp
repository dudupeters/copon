#include "IApplication.h"
#include "ISchedulerStrategy.h"
#include "IScheduler.h"

Application::Application(Scheduler* scheduler){
	this->scheduler = scheduler;	

	//startApplication();
}

Application::Application(int schedulerStrategy){
	scheduler = new Scheduler(schedulerStrategy);	
	//startApplication();
}

Application::~Application(void){

}

void Application::startApplication(){
	initStartApplicationComponents();
	initFactBase();
	initSharedPremises();
	configureStartApplicationAction();
	initRules();
	approveStartRule();
	codeApplication();
	//scheduler->execute();	
}

void Application::addInstigationToStartAction(Instigation *instigation){
	acStartApplication->addInstigation(instigation);
}

void Application::initStartApplicationComponents(){
	atStartApplication = new Boolean(NULL, false);

	/*prStartApplication = new Premise(atStartApplication, Boolean::TRUE, Premise::EQUAL, false);
	cdStartApplication = new Condition(Condition::CONJUNCTION);
	cdStartApplication->addPremise(prStartApplication);
	acStartApplication = new Action();
	rlStartApplication = new Rule("rlStartApplication", scheduler, cdStartApplication, acStartApplication);*/

}

void Application::approveStartRule(){
	atStartApplication->setValue(true);
}

Scheduler* Application::getScheduler(){
	return scheduler;
}

