#include "IAction.h"

Action::Action(void){	
	
	instigationList = new MyList<Instigation>();
}

Action::~Action(void){

  int tam = instigationList->getSize();
  
  for (int i = 0; i < tam; i++)
  {
      delete instigationList->getElement(i);
  }  
  instigationList->deleteAll();
    
}

void Action::setRule(Rule* rule){
	this->rule = rule;
}

void Action::execute(){		
	MyList<Instigation>::Iterator It;   
	for( It.setCurrent( instigationList->getFirst() ); It.getInfo() != NULL; It.Next() )
		It.getInfo()->execute();
	setExecuted(true);
}

bool Action::isExecuted(){
	return executed;
}

void Action::setExecuted(bool executed){
	this->executed = executed;
}

void Action::addInstigation(Instigation* instigation){
	instigationList->push_back(instigation);	
	
}
