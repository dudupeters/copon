#include "IIntegerPlus.h"

/**
* IntegerPlus default constructor
*/
IntegerPlus::IntegerPlus() : Integer() {}

/**
* IntegerPlus constructor
* @param value a initial Integer value
*/
IntegerPlus::IntegerPlus(int value): Integer(value) {}

/**
* IntegerPlus constructor
* @param fact reference to the FBE of the Attribute Integer
* @param value a initial Integer value
*/
IntegerPlus::IntegerPlus(FBE* fact, int value): Integer(fact, value) {}

/**
* IntegerPlus constructor
* @param address to a Integer value
*/
IntegerPlus::IntegerPlus(Integer& attr): Integer(attr) {}

/**
* IntegerPlus default destructor
*/
IntegerPlus::~IntegerPlus(void) {}

/**
* Compound assignment operator overload (++).
* Ex: ++(*atInteger);
*/
void IntegerPlus::operator ++ ()
{
	this->setValue( myInteger + 1 );
}

/**
* Compound assignment operator overload (--).
* Ex: --(*atInteger);
*/
void IntegerPlus::operator -- ()
{
	this->setValue( myInteger - 1 );
}

/**
* Adds a value to the current value
* @param value to be added
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void IntegerPlus::add(Integer* value, int flag){
	setValue( ( myInteger + value->getValue() ), flag );
}

/**
* Adds a value to the current value
* @param value to be added
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void IntegerPlus::add(int value, int flag){
	setValue( ( myInteger + value ), flag );
}

/**
* Subtracts a value to the current value
* @param value to be subtracted
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void IntegerPlus::subtract(Integer* value, int flag){
	setValue( ( myInteger - value->getValue() ), flag );
}

/**
* Subtracts a value to the current value
* @param value to be subtracted
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void IntegerPlus::subtract(int value, int flag){
	setValue( ( myInteger - value ), flag );
}

/**
* Multiplies a value to the current value
* @param value to be multiplied
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void IntegerPlus::multiply(Integer* value, int flag){
	setValue( ( myInteger * value->getValue() ), flag );
}

/**
* Multiplies a value to the current value
* @param value to be multiplied
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void IntegerPlus::multiply(int value, int flag){
	setValue( ( myInteger * value ), flag );
}

/**
* Divides a value to the current value
* @param value to be divided
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void IntegerPlus::divide(Integer* value, int flag){
	int tmp = value->getValue();
	if ( tmp == 0 )
		setValue( 0, flag );
	else
		setValue( ( myInteger / tmp ), flag );
}

/**
* Divides a value to the current value
* @param value to be divided
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void IntegerPlus::divide(int value, int flag){
	if ( value == 0 )
		setValue( 0, flag );
	else
		setValue( ( myInteger / value ), flag );
}
