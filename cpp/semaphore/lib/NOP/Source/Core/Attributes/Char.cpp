#include "IChar.h"

/**
* Char default constructor
*/
Char::Char(){
	this->myChar = ' ';
}

/**
* Char constructor
* @param value a initial Char value
*/
Char::Char(char value): Attribute(){
	this->myChar = (char)value;
}

/**
* Char constructor
* @param fact reference to the FBE of the Attribute Char
* @param value a initial Char value
*/
Char::Char(FBE* fact, char value): Attribute(fact){
	this->myChar = (char)value;
}

/**
* Char default destructor
*/
Char::~Char(void){
}

/**
* Returns the Char primitive value
* @param myChar a Char primitive value/state
*/
double Char::getValue(){
	return this->myChar;
} 

/**
* Update the Char Attribute value. 
* This method start a notification flow if the assigned value is different of the last value.
* @param value a primitive Char value to be assigned
*/
void Char::setValue(char value){
	if(this->myChar != value) {
		this->myChar = value;
		notifyPremises();
	}
}

/**
* Update the Char Attribute value.
* This method start a notification flow if the assigned value is different of the last value.
* @param value a Char object
*/
void Char::setValue(Attribute *value){
	setValue(((Char*)value)->getValue());
}

/**
* Update the Char Attribute value.
* This method start a notification flow if the assigned value is different or even the same than the last one.
* This method is used when the renotification mechanism is desired.
* @param value a primitive Char value to be assigned
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void Char::setValue(char value, int flag){
	
	if (this->myChar != value){
		this->myChar = value;
		if (flag != Attribute::NO_NOTIFY)
			notifyPremises();
	}
	else if (flag == Attribute::RENOTIFY)
		renotifyPremises();
	
}

/**
* Update the Char Attribute value.
* This method start a notification flow if the assigned value is different or even the same than the last one.
* This method is used when the renotification mechanism is desired.
* @param value a Char value to be assigned
* @param flag should be passed the value of the
*        - Attribute::RENOTIFY, it denotes the desire of the renotification mechanism
*        - Attribute::NO_NOTIFY, it denotes the desire of not notifying the value change
*/
void Char::setValue(Attribute *value, int flag){
    setValue( ( (Char*) value)->getValue(), flag);
}

/**
* Verify if the assigned value(object) is equal to the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Char::isEqual(Attribute *attributeB){
	if( myChar == ( (Char*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is greater than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Char::isGreaterThan(Attribute *attributeB){
	if( myChar > ((Char*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is smaller than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Char::isSmallerThan(Attribute *attributeB){
	if( myChar < ((Char*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is greater or equal than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Char::isGreaterOrEqualThan(Attribute *attributeB){
	if( myChar >= ((Char*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is smaller than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Char::isSmallerOrEqualThan(Attribute *attributeB){
	if( myChar <= ((Char*)attributeB)->getValue())
		return true;
		return false;
}

/**
* Verify if the assigned value(object) is different than the actual value(object)
* @param attributeB a object value to be compared
* @return the result of the comparison
*/
bool Char::isDifferent(Attribute *attributeB){
	if( myChar != ((Char*)attributeB)->getValue())
		return true;
		return false;
}
