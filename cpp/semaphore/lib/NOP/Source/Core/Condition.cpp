#include "ICondition.h"
#include "ISubCondition.h"


#include <fstream>
using std::ofstream;
using std::ifstream;

Condition::Condition(LogicalOperator* logicalOperator){
	this->logicalOperator = logicalOperator;
	this->amountElements = 0;
	this->amountElementsTrue = 0;	
	this->setLogicValue(false);	
	premiseList = new MyList<Premise>();
}

Condition::Condition(int operattor){
	this->amountElements = 0;
	this->amountElementsTrue = 0;	

	if(operattor == CONJUNCTION){
		this->logicalOperator = new Conjunction();
	}
	else if(operattor == DISJUNCTION){
		this->logicalOperator = new Disjunction();
	}
	else if (operattor == SINGLE){
		this->logicalOperator = new Single();
	}
	setLogicValue(false);
	
	premiseList = new MyList<Premise>();
	
}

Condition::~Condition(void){
	int tam = premiseList->getSize();
    for (int i = 0; i < tam; i++)
    {
      Premise* premise = premiseList->getElement(i);
      if (premise->getNumOfConditions() == 1) {
        delete premise; 
      }
      else
      {
        premise->removeCondition(this); 
      }      
    }
}

void Condition::logicCalculus(){

	bool result = logicalOperator->isTrue(amountElements, amountElementsTrue);	
	if(result != this->getLogicValue()){
		setLogicValue(result);
		if(getLogicValue()){
			rule->approved();
		}
		else{
			rule->reproved();
		}
	}
}

void Condition::renotifyLogicCalculus(){
	if(getLogicValue()){
		rule->approved();
	}
}

bool Condition::renotifyLogicCalculus2(){
	if(getLogicValue()){
		rule->approved();
		return true;
	}
	else{
		return false;
	}
}

bool Condition::getLogicValue(){
	return logicValue;
}

void Condition::setLogicValue(bool logicValue){
	this->logicValue = logicValue;
}

void Condition::incrementElementsTrue(){
	this->amountElementsTrue++;
}

void Condition::decrementElementsTrue(){
	this->amountElementsTrue--;
}

void Condition::incrementElements(){
	this->amountElements++;
}

void Condition::decrementElements(){
	this->amountElements--;
}

void Condition::addSubCondition(SubCondition* anySubCondition){

	anySubCondition->addCondition(this);
	incrementElements();
	bool subConditionLogicValue = anySubCondition->calculateLogicalValue();
	if(subConditionLogicValue){
		incrementElementsTrue();
		anySubCondition->setLogicValue(subConditionLogicValue);
	}
}

void Condition::addPremise(Premise* anyPremise){
	anyPremise->addCondition(this);
	premiseList->push_back(anyPremise);
	incrementElements();
	//bool premiseLogicCalculus = anyPremise->logicCalculus(); // Agora, o c�lculo l�gico da premissa � feito no momento de sua cria��o.
	if(anyPremise->getLogicValue()){		
		incrementElementsTrue();
	}
}

void Condition::setRule(Rule *rule){
	this->rule = rule;
}

int Condition::getAmountElements(){
	return amountElements;
}
