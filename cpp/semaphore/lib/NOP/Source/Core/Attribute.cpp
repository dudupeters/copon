#include "IAttribute.h"
#include "IPremise.h"

Attribute::Attribute(FBE* fact, Attribute* value){  
	this->fact = fact;
	this->value = value;
	premisesList = new MyList<Premise>();
}

Attribute::Attribute(FBE *fact){
	this->fact = fact;	
	premisesList = new MyList<Premise>();
}

Attribute::Attribute(Attribute* attr) {
  this->fact = attr->fact;
  this->value = attr->value;
  premisesList = new MyList<Premise>();
  
}

Attribute::Attribute(){ 
	premisesList = new MyList<Premise>();
}    

Attribute::~Attribute(){ 
}    

FBE* Attribute::getFact(){ 
	return this->fact; 
}

void Attribute::setFact(FBE *fact){
    this->fact = fact;
}

void Attribute::changeValue(){
		notifyPremises();		
}

void Attribute::notifyPremises(){
    Premise* premiseTmp = 0; 
	MyList<Premise>::Iterator It;
	for( It.setCurrent( premisesList->getFirst() ); It.getInfo() != NULL; It.Next() )
	{
		premiseTmp = It.getInfo();
		if(premiseTmp->isExclusive())
			premiseTmp->notifyConditionsAboutPremiseExclusive();	   
		else
			premiseTmp->notifyConditions();	   			
	}
}


void Attribute::renotifyPremises(){
    Premise* premiseTmp = 0; 
	MyList<Premise>::Iterator It;
	for( It.setCurrent( premisesList->getFirst() ); It.getInfo() != NULL; It.Next() )
	{	
		premiseTmp = It.getInfo();
		if(premiseTmp->isExclusive())
			premiseTmp->renotifyConditionsAboutPremiseExclusive();	   
		else
			premiseTmp->renotifyConditions();	   	   
	}    
}


void Attribute::addPremise(Premise* premise){	
	premisesList->push_back(premise);	
}

void Attribute::removePremise(Premise* premise)
{
   premisesList->removeElement(premise);
}

Premise* Attribute::getPremise(int index){
	return NULL;
}

char* Attribute::getName(){
	return name;
}

void Attribute::setName(char* name){
	this->name = name;
}

int Attribute::getId(){
	return this->id;
}

void Attribute::setId(int id){
	this->id = id;
}
