#ifndef _ICONCRETEAPPLICATION_H_
#define _ICONCRETEAPPLICATION_H_

#include "IApplication.h"

/**
 * It is a example class. This class represents any Main class of a application.
 * This class extends the class Application in order to override its abstract methods. 
 * In this class, it is possible to declare de FBEs, the Rules, and some initial code of the application  
 * @author  Roni Fabio Banaszewski
 * @version  1.0
 * @see Application
 */

class ConcreteApplication :	public Application {

public:
	ConcreteApplication(Scheduler* scheduler);
	ConcreteApplication(int schedulerStrategy);
	~ConcreteApplication(void);
	
	void initRules();
	void initFactBase();
	void initSharedPremises();
	void configureStartApplicationAction();
	void codeApplication();
};

#endif 
