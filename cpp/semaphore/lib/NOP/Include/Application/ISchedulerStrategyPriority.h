#ifndef _ISCHEDULERSTRATEGYPRIORITY_H_
#define _ISCHEDULERSTRATEGYPRIORITY_H_

#include "IRule.h"
#include "ISchedulerStrategy.h"

/**
 * (IN CONSTRUCTION)<BR>
 * This class represents the Conflict Resolution Strategy based in priorities.  
 * The approved Rules are inserted in the data structure and are organized according with their priorities.
 * @author  Roni Fabio Banaszewski
 * @version  1.0
 * @see SchedulerStrategy, SchedulerStrategyDepth, SchedulerStrategyBreadth
 */

class SchedulerStrategyPriority : public SchedulerStrategy{

private:
	int amountOfRules;
	Rule* rulesToExecute [101];

public:
	SchedulerStrategyPriority(void);
	~SchedulerStrategyPriority(void);
		
	void execute();
	void addRuleToExecutionList(Rule* rule);
	void removeRuleToExecutionList(Rule* rule);
	int getAmountOfRules();
	void incrementAmountOfRules();
	void decrementAmountOfRules();
};

#endif
