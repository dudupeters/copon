#ifndef _ISINGLE_H_
#define _ISINGLE_H_

#include "ILogicalOperator.h"

/**
 * This class represents a logical operator used when a Condition is connected to only one Premise.
 * @author Roni Fabio Banaszewski
 * @version 1.0
 * @see Condition
 */

class Single : public LogicalOperator {

public:
	Single(void);
	~Single(void);
    
	/**
    * Verify if the respective Condition is true or approved.
    */
	bool isTrue(int amountElements, int amountElementsTrue);
};

#endif
