#ifndef _IDISJUNCTION_H_
#define _IDISJUNCTION_H_

#include "ILogicalOperator.h"

/**
 * This class represents a logical operator of disjunction in a Condition.
 * @author Roni Fabio Banaszewski
 * @version 1.0
 * @see Condition
 */

class Disjunction : public LogicalOperator{
public:
	Disjunction(void);
	~Disjunction(void);
   /**
    * Verify if the respective Condition is true or approved.
    */
	bool isTrue(int amountElements, int amountElementsTrue);
};

#endif
