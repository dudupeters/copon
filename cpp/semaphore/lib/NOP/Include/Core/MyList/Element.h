#pragma once

template<class TIPO>
class Element
{
private:
	Element<TIPO>* pnext;
	Element<TIPO>* pant;
	TIPO *info;
	
public:
	Element(void);
	~Element(void);

	void setprox(Element<TIPO> *pnext);
	Element<TIPO>* getprox();

	void setant(Element<TIPO> *pant);
	Element<TIPO>* getant();

	void setinfo(TIPO *info);
	TIPO* getinfo();

	
};

	template <class TIPO>
	Element<TIPO>::Element(){}

	template <class TIPO>
	Element<TIPO>::~Element(){}


	template<class TIPO>
	void Element<TIPO>::setprox(Element<TIPO> *pnext){
		this->pnext = pnext;
	}

	template<class TIPO>
	Element<TIPO>* Element<TIPO>::getprox(){
		return pnext;
	}

	template<class TIPO>
	void Element<TIPO>::setant(Element<TIPO> *pant){
		this->pant = pant;
	}

	template<class TIPO>
	Element<TIPO>* Element<TIPO>::getant(){
		return pant;
	}

	template<class TIPO>
	void Element<TIPO>::setinfo(TIPO* info){
		this->info = info;
	}

	template<class TIPO>
	TIPO* Element<TIPO>::getinfo(){
		return this->info;
	}

	




