#ifndef _IMETHOD_H_
#define _IMETHOD_H_

#include "MyList.h"

class FBE;
class Attribute;

class Method{

public:
	Method();
	Method(FBE *fact);
	~Method();
	
	virtual void execute(){};
	virtual void execute(MyList< Attribute > *parameters){};
	virtual void execute(int parameter){};

protected:
	bool executing;
	FBE* fact;
};

#endif
