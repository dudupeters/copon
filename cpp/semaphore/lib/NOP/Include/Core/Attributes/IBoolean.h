#ifndef _IBOOLEAN_H_
#define _IBOOLEAN_H_

#include "IAttribute.h"

/**
 * This class represents the primitive Boolean data type with capacities of reactiveness, decoupling and collaborations by notifications.
 * @author  Roni Fabio Banaszewski
 * @version  1.0
 */
class Boolean : public Attribute{

public:
    static Boolean* TRUE_NOP;
	static Boolean* FALSE_NOP; 

public:
	Boolean();
	Boolean(bool value);
	Boolean(FBE* fact, bool value);	
	
	~Boolean();	
	
	bool getValue();
	void setValue(bool value);
	void setValue(bool value, int flag); //flag: Attribute::RENOTIFY / Attribute::NO_NOTIFY
	void setValue(Attribute* value);
	void setValue(Attribute* value, int flag);
	bool isEqual(Attribute* AttributeB);
	bool isGreaterThan(Attribute* attributeB);
	bool isSmallerThan(Attribute* attributeB);
	bool isGreaterOrEqualThan(Attribute* attributeB);
	bool isSmallerOrEqualThan(Attribute* attributeB);
	bool isDifferent(Attribute* attributeB);
	
private:
	bool myBool;
};

#endif
