#ifndef _IINTEGER_PLUS_H_
#define _IINTEGER_PLUS_H_

#include "IInteger.h"

class IntegerPlus : public Integer {

public:

	IntegerPlus();
	IntegerPlus(int value);
	IntegerPlus(FBE* fact, int value);	
	IntegerPlus(Integer& attr);
	~IntegerPlus(void);

	void operator ++ ();
	void operator -- ();
	
	void add(Integer* value, int flag = 0);
	void add(int value, int flag = 0);

	void subtract(Integer* value, int flag = 0);
	void subtract(int value, int flag = 0);
	
	void multiply(Integer* value, int flag = 0);
	void multiply(int value, int flag = 0);
	
	void divide(Integer* value, int flag = 0);
	void divide(int value, int flag = 0);

};

#endif
