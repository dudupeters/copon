#ifndef _IDOUBLE_H_
#define _IDOUBLE_H_

#include "IAttribute.h"

/**
 * This class represents the primitive double data type with capacities of reactiveness, decoupling and collaborations by notifications.
 * @author  Roni Fabio Banaszewski
 * @version  1.0
 */

class Double : public Attribute {

public:

	Double();
	Double(double value);
	Double(FBE* fact, double value);
	Double(Double& attr);
	~Double(void);

	double getValue();
	void setValue(double value);
	void setValue(Attribute* value);
	void setValue(double value, int flag);
	void setValue(Attribute* value, int flag);
	bool isEqual(Attribute* AttributeB);
	bool isGreaterThan(Attribute* attributeB);
	bool isSmallerThan(Attribute* attributeB);
	bool isGreaterOrEqualThan(Attribute* attributeB);
	bool isSmallerOrEqualThan(Attribute* attributeB);
	bool isDifferent(Attribute* attributeB);
	
protected:

	double myDouble;

};

#endif
