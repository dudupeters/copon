#ifndef _IINTEGER_H_
#define _IINTEGER_H_

#include "IAttribute.h"

/**
 * This class represents the primitive int data type with capacities of reactiveness, decoupling and collaborations by notifications.
 * @author  Roni Fabio Banaszewski
 * @version  1.0
 */

class Integer : public Attribute {

public:

	Integer();
	Integer(int value);
	Integer(FBE* fact, int value);	
	Integer(Integer& attr);
	~Integer(void);

	int getValue();
	void setValue(int value);
	void setValue(Attribute* value);
	void setValue(int value, int flag);//flag: Attribute::RENOTIFY
	void setValue(Attribute* value, int flag);
	bool isEqual(Attribute* AttributeB);
	bool isGreaterThan(Attribute* attributeB);
	bool isSmallerThan(Attribute* attributeB);
	bool isGreaterOrEqualThan(Attribute* attributeB);
	bool isSmallerOrEqualThan(Attribute* attributeB);
	bool isDifferent(Attribute* attributeB);
	
protected:
	
	int myInteger;

};

#endif
