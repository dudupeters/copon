#ifndef _IATTRIBUTE_H_
#define _IATTRIBUTE_H_

#include "IFBE.h"
#include "MyList.h"

//using namespace std;

#include <cstring>
using std::strcpy;

class Premise;

class Attribute {

private:
	  char* name;	  
	  int id;
	  Attribute* value;	  	  	  
	  MyList<Premise> *premisesList;	    

public:
      const static int RENOTIFY = 1;
	  const static int NO_NOTIFY = 2;

public:	  
	  Attribute();	  	
	  Attribute(FBE* fact, Attribute* value);
	  Attribute(FBE* fact);
	  Attribute(Attribute* attr);
	  ~Attribute();

	  FBE* getFact();

  	  void setFact(FBE* fact);
	  void notifyPremises(); //Notifica as Premises
	  void renotifyPremises(); //Notifica as Premises com a renotificação
	  void addPremise(Premise* premise); //Adiciona uma Premise a ser notificada pelo Attribute
	  void removePremise(Premise* premise);
	  
	  Premise* getPremise(int index);//Recupera uma das Premises relacionadas ao Attribute
	  
	  char* getName();
	  void setName(char*);  
	  int getId();
	  void setId(int id);
	  int getType(){ 
		  return type; 
	  } 

	  //Virtuals
	  virtual void setValue(Attribute* value){};
	  virtual bool isEqual(Attribute* attributeB){return false;};
	  virtual bool isGreaterThan(Attribute* attributeB){return false;};
	  virtual bool isSmallerThan(Attribute* attributeB){return false;};     
	  virtual bool isGreaterOrEqualThan(Attribute* attributeB){return false;};
	  virtual bool isSmallerOrEqualThan(Attribute* attributeB){return false;};
	  virtual bool isDifferent(Attribute* attributeB){return false;};

protected:
    FBE* fact;
    int type; 	              
   	
    void changeValue();	
	virtual bool isValueChange(Attribute* value){
		return false;
	}     
};

#endif
