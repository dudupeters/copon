#ifndef _IRULEDERIVED_H_
#define _IRULEDERIVED_H_

#include "ICondition.h"
#include "IPremise.h"
//#include <map>
//#include <vector>
#include "MyList.h"

class RuleDerived :	public Rule{

private: 
	SubCondition* subCondition;
	//map < int, Attribute*> attributes;
	//vector<Attribute*> attributes;
	MyList<Attribute> *attributes;

public:
	RuleDerived(char* name, Scheduler* scheduler,int logicalOperator);
	~RuleDerived(void);
	
	void addPremise(Attribute* attributeA, Attribute* attributeB, int operattor, bool exclusive, int attributesToSave  = Premise::NO_ONE);
	void addPremise(Attribute* attributeA, bool atBoolean, int operattor, bool exclusive, int attributesToSave = Premise::NO_ONE);
	void addPremise(Attribute* attributeA, int atInteger, int operattor, bool exclusive, int attributesToSave = Premise::NO_ONE);
	void addPremise(Attribute* attributeA, double atDouble, int operattor, bool exclusive, int attributesToSave = Premise::NO_ONE);
	void addPremise(Premise* premise);
	void addSubCondition(int logicalOperator, bool exclusive);
	//void addSubCondition(LogicalOperator logicalOperator, bool exclusive);
	void addPremiseToSubCondition(Attribute* attributeA, Attribute* attributeB, int operattor, bool exclusive);
	void addPremiseToSubCondition(Attribute* attributeA, bool atBoolean, int operattor, bool exclusive);
	void addPremiseToSubCondition(Attribute* attributeA, int atInteger, int operattor, bool exclusive);
	void addPremiseToSubCondition(Attribute* attributeA, double atDouble, int operattor, bool exclusive);
	void addPremiseToSubCondition(Premise* premise);
	
	Attribute* getAttribute(Premise* premise, int choosePosition);
};

#endif
