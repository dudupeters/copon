#ifndef _IINSTIGATION_H_
#define _IINSTIGATION_H_

#include "IAttribute.h"
#include "IBoolean.h"
#include "IInteger.h"
#include "IDouble.h"
#include "IChar.h"
#include "IString.h"
#include "MyList.h"

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

class Method;

class Instigation {

private:
	
	Attribute *attribute;
	Method *method;	
	
	bool bValue;
	int iValue;
	double dValue;
	char cValue;
	char* sValue;
	bool isAttribute;
	
	MyList< Attribute > *parameters;

	//Modificado por Sim�o em 17/08/2009
	//enum AttributeType {BOOLEAN, INTEGER, DOUBLE, CHAR, STRING};
	enum AttributeType {BOOLEAN_NOP, INTEGER_NOP, DOUBLE_NOP, CHAR_NOP, STRING_NOP};
	AttributeType attributeType;

public:
	Instigation(Method *method);
	Instigation(Method* method, MyList< Attribute > *attributes);
	Instigation(Boolean* attribute, bool value);
	Instigation(Integer* attribute, int value);
	Instigation(Double* attribute, double value);	
	Instigation(Char* attribute, char value);
	Instigation(String* attribute, char* value);
	~Instigation();
	void execute();	

    // Adicionado por Fernando Witt 18-02-2010
	Method* getMethod(){ return method; }
};

#endif
