#ifndef _IMETHODDERIVED_H_
#define _IMETHODERIVED_H_

#include "IMethod.h"

class MethodDerived : public Method {

public:
	MethodDerived(void);
	MethodDerived(FBE *fact);
	~MethodDerived(void);

	void execute();
};

#endif
