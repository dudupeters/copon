#include "system.h"
#include <stdio.h>
#include <iostream>
#include <stddef.h>
#include <unistd.h>
#include "altera_avalon_pio_regs.h"
#include "alt_types.h"
#include "io.h"
#include "copon_memory_map.h"

using namespace std;

int main()
{
	cout << "COPON test\n\n";

	int status0;

	status0 = IORD(PON_COPROCESSOR_0_BASE, STATUS0);
	cout << "status0: " << status0 << endl;

	IOWR(PON_COPROCESSOR_0_BASE, STATUS0, 123);
	IOWR(PON_COPROCESSOR_0_BASE, PREMISE0_CONST_VALUE, 456);
	IOWR(PON_COPROCESSOR_0_BASE, RULE_ADDRESS, 789);

	cout << "Populate registers:\n";

	int register_value, i;
	for (i = 0; i < 17; ++i) {
		IOWR(PON_COPROCESSOR_0_BASE, i, (i*i));
	}

	cout << "Dump registers:\n";

	for (i = 0; i < 17; ++i) {
//		IOWR(PON_COPROCESSOR_0_BASE, i, (i*i));
		register_value = IORD(PON_COPROCESSOR_0_BASE, i);
		cout << i << ": " << register_value << endl;
	}

}
