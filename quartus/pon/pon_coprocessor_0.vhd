-- pon_coprocessor_0.vhd

-- This file was auto-generated as part of a generation operation.
-- If you edit it your changes will probably be lost.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity pon_coprocessor_0 is
	port (
		reset        : in  std_logic                     := '0';             --     reset.reset
		address      : in  std_logic_vector(4 downto 0)  := (others => '0'); -- copon_cpu.address
		chip_select  : in  std_logic                     := '0';             --          .chipselect
		write_enable : in  std_logic                     := '0';             --          .write
		write_data   : in  std_logic_vector(31 downto 0) := (others => '0'); --          .writedata
		read_enable  : in  std_logic                     := '0';             --          .read
		read_data    : out std_logic_vector(31 downto 0);                    --          .readdata
		clk          : in  std_logic                     := '0'              --     clock.clk
	);
end entity pon_coprocessor_0;

architecture rtl of pon_coprocessor_0 is
	component avalon_copon is
		generic (
			ADDRESS_WIDTH : integer := 5;
			DATA_WIDTH    : integer := 32
		);
		port (
			reset        : in  std_logic                     := 'X';             -- reset
			address      : in  std_logic_vector(4 downto 0)  := (others => 'X'); -- address
			chip_select  : in  std_logic                     := 'X';             -- chipselect
			write_enable : in  std_logic                     := 'X';             -- write
			write_data   : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
			read_enable  : in  std_logic                     := 'X';             -- read
			read_data    : out std_logic_vector(31 downto 0);                    -- readdata
			clk          : in  std_logic                     := 'X'              -- clk
		);
	end component avalon_copon;

begin

	pon_coprocessor_0 : component avalon_copon
		generic map (
			ADDRESS_WIDTH => 5,
			DATA_WIDTH    => 32
		)
		port map (
			reset        => reset,        --     reset.reset
			address      => address,      -- copon_cpu.address
			chip_select  => chip_select,  --          .chipselect
			write_enable => write_enable, --          .write
			write_data   => write_data,   --          .writedata
			read_enable  => read_enable,  --          .read
			read_data    => read_data,    --          .readdata
			clk          => clk           --     clock.clk
		);

end architecture rtl; -- of pon_coprocessor_0
