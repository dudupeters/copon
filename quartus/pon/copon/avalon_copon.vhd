library ieee;
use ieee.std_logic_1164.all;

entity avalon_copon is
   port (
      -- Avalon clock input interface
      clk, reset: in  std_logic;
      -- Avalon MM slave interface
      address: in std_logic_vector(2 downto 0);    
      chip_select: in std_logic; 
      write_enable: in std_logic;
      write_data: in std_logic_vector(W-1 downto 0);
      read_enable: in std_logic;
      read_data: out std_logic_vector(W-1 downto 0)
   );
end avalon_copon;

architecture arch of avalon_copon is
   
begin

   pon_coprocessor: entity work.copon
   port map(
			clk => clk, 
			reset => reset, 
      address => address,
      chip_select => chip_select,			
      write_enable => write_enable,
      write_data => write_data,
      read_enable => read_enable,
      read_data => read_data
		);
   
end arch;
