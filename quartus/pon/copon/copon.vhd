library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity copon is
	constant ADDRESS_WIDTH: integer := 5;
	constant DATA_WIDTH: integer := 5;
	constant REGISTERS_COUNT: integer := 17;
    port(
      clk, reset: in std_logic;
			address: in std_logic_vector(ADDRESS_WIDTH-1 downto 0);
			chip_select: in std_logic;
			write_enable: in std_logic;
			write_data: in std_logic_vector(DATA_WIDTH-1 downto 0);
			read_enable: in std_logic;
			read_data: out std_logic_vector(DATA_WIDTH-1 downto 0)
    );
end copon;

architecture arch of copon is

	subtype word is std_logic_vector(DATA_WIDTH-1 downto 0);
	type memory is array (0 to REGISTERS_COUNT-1) of word;
	signal registers: memory;

begin

  process(clk,reset) begin
		if reset='1' then
			registers <= (others => '0');
		elsif (clk'event and clk='1') then
			if write_enable = '1' and chip_select = '1' then 
				registers(address) <=  write_data;
			end if;
			if read_enable = '1' and chip_select = '1' then
				read_data <= registers(address);
			end if;
		end if;
  end process;

end arch;