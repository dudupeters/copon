  --Example instantiation for system 'pon'
  pon_inst : pon
    port map(
      LCD_E_from_the_lcd_16207_0 => LCD_E_from_the_lcd_16207_0,
      LCD_RS_from_the_lcd_16207_0 => LCD_RS_from_the_lcd_16207_0,
      LCD_RW_from_the_lcd_16207_0 => LCD_RW_from_the_lcd_16207_0,
      LCD_data_to_and_from_the_lcd_16207_0 => LCD_data_to_and_from_the_lcd_16207_0,
      oSEG0_from_the_SEG7_Display => oSEG0_from_the_SEG7_Display,
      oSEG1_from_the_SEG7_Display => oSEG1_from_the_SEG7_Display,
      oSEG2_from_the_SEG7_Display => oSEG2_from_the_SEG7_Display,
      oSEG3_from_the_SEG7_Display => oSEG3_from_the_SEG7_Display,
      oSEG4_from_the_SEG7_Display => oSEG4_from_the_SEG7_Display,
      oSEG5_from_the_SEG7_Display => oSEG5_from_the_SEG7_Display,
      oSEG6_from_the_SEG7_Display => oSEG6_from_the_SEG7_Display,
      oSEG7_from_the_SEG7_Display => oSEG7_from_the_SEG7_Display,
      out_port_from_the_led_green => out_port_from_the_led_green,
      out_port_from_the_led_red => out_port_from_the_led_red,
      zs_addr_from_the_sdram_0 => zs_addr_from_the_sdram_0,
      zs_ba_from_the_sdram_0 => zs_ba_from_the_sdram_0,
      zs_cas_n_from_the_sdram_0 => zs_cas_n_from_the_sdram_0,
      zs_cke_from_the_sdram_0 => zs_cke_from_the_sdram_0,
      zs_cs_n_from_the_sdram_0 => zs_cs_n_from_the_sdram_0,
      zs_dq_to_and_from_the_sdram_0 => zs_dq_to_and_from_the_sdram_0,
      zs_dqm_from_the_sdram_0 => zs_dqm_from_the_sdram_0,
      zs_ras_n_from_the_sdram_0 => zs_ras_n_from_the_sdram_0,
      zs_we_n_from_the_sdram_0 => zs_we_n_from_the_sdram_0,
      clk => clk,
      clk_50 => clk_50,
      in_port_to_the_button_pio => in_port_to_the_button_pio,
      in_port_to_the_switch_pio => in_port_to_the_switch_pio,
      reset_n => reset_n
    );


